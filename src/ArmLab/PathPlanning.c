#include "gui.h"
#include "PathPlanning.h"

#define MAX_ITR 1000

//#define PF_DEBUG 
//#define LINESEARCH

WayPoints_t *waypoints;
Trajectory_t *trajectory;
obstacle_t *OBS = NULL;

extern double max_servo[4];
extern double min_servo[4];


double padding_cyl = 4.2;  // padding for cylinder radius
double h_padd_cyl  = 3.0;
double h_padd      = 1;  // padding for heights
double padding_O   = 1;
double padding_I   = 0.3;
double padding_O4  = 1;  //padding for link 4



double FP_gui[3] = {0,0,0};

double time_step  = 0.01;

double F_TOTAL_ATT[3] = {0,0,0};
double F_TOTAL_REP[3] = {0,0,0};

double ERROR_PF = 10; 

double pf_objfun(double *q,double *qf,double *Xf,obstacle_t *obs,double *T_total,unsigned char ex_pf){

    double dlen   = 6;      //influence  
    double zeta   = 60;    //strength
     
    double rho1   = 5;     //Cylinder influence
    double rho2   = 5;    //Annulus influence
    double rho3   = 5;    //plane influence
    //double rho3   = 5;

    double eta1   = 4;//20;    //cylinder
    double eta2   = 50;   //annulus
    double eta3   = 20;    //plane
    
    double rho;
    double eta;
  
    /** World co-ordinates of current and goal config */
    double *joint1,*joint2,*joint3,*joint4;
    double *joint2g,*joint3g,*joint4g;
      
    /** 6 X 4 Jacobian */
    gsl_matrix *Jacobian2;
    gsl_matrix *Jacobian3;
    gsl_matrix *Jacobian4;

    gsl_matrix *Jacobianlf2;
    gsl_matrix *Jacobianlf3;
    gsl_matrix *Jacobianlf4;
    	    	 	 
    /** Attractive and repulsive torques */
    double t_att4[4]    = {0,0,0,0};
    double t_att2[4]    = {0,0,0,0};
    double t_att3[4]    = {0,0,0,0};
    double t_att[4]     = {0,0,0,0};

    double t_rep[4]    = {0,0,0,0};

    double t_rep2[4]   = {0,0,0,0};
    double t_rep3[4]   = {0,0,0,0};
    double t_rep4[4]   = {0,0,0,0};

    double t_rep_f2[4] = { 0, 0, 0, 0 };
    double t_rep_f3[4] = { 0, 0, 0, 0 };
    double t_rep_f4[4] = { 0, 0, 0, 0 };
      
    obstacle_t *head  = obs;
          
    // Current positions of DH frames
    joint1    = tip_pos(q[0],q[1],q[2],q[3],1);
    joint2    = tip_pos(q[0],q[1],q[2],q[3],2);
    joint3    = tip_pos(q[0],q[1],q[2],q[3],3);
    joint4    = tip_pos(q[0],q[1],q[2],q[3],4); // End effector
   
#ifdef PF_DEBUG
    printf("\n\n\n************* New Iteration *******************\n");
    printf("tip pos = %f %f %f\n",joint4[0],joint4[1],joint4[2]);
  
    printf("angles = %f %f %f %f\n",q[0],q[1],q[2],q[3]);
    printf("tip position error = %f",getError(q,qf,Xf,0));
    printf("joint 3 pos = %f %f %f\n",joint3[0],joint3[1],joint3[2]);
    printf("joint 2 pos = %f %f %f\n",joint2[0],joint2[1],joint2[2]);
    cc_check(q,obs);
#endif

    double U_att=0,U_rep2=0,U_rep3=0,U_rep4=0;
    double U_rep4f=0, U_rep3f=0, U_rep2f=0;

    F_TOTAL_ATT[0] = 0;
    F_TOTAL_ATT[1] = 0;
    F_TOTAL_ATT[2] = 0;
    
    F_TOTAL_REP[0] = 0;
    F_TOTAL_REP[1] = 0;
    F_TOTAL_REP[2] = 0;

	
    // Goal positions of DH frames
    if(ex_pf){
	joint2g   = tip_pos(qf[0],qf[1],qf[2],qf[3],2);
	joint3g   = tip_pos(qf[0],qf[1],qf[2],qf[3],3);
	joint4g   = tip_pos(qf[0],qf[1],qf[2],qf[3],4);
    }
    else{
	joint4g   = Xf;
    }

    // Jacobian matrices corresponding to each DH frame origin
    Jacobian2     = Compute_Jacobian(q,joint1,joint2,joint3,joint4,joint2,2); 
    Jacobian3     = Compute_Jacobian(q,joint1,joint2,joint3,joint4,joint3,3);
    Jacobian4     = Compute_Jacobian(q,joint1,joint2,joint3,joint4,joint4,4);

    // Get attractive torques for end effector

    
    if(ex_pf){
	U_att += pf_att(joint2,joint2g,Jacobian2,dlen,zeta,t_att2);
	U_att += pf_att(joint3,joint3g,Jacobian3,dlen,zeta,t_att3);
	U_att += pf_att(joint4,joint4g,Jacobian4,dlen,zeta,t_att4);

	t_att[0] = t_att2[0] + t_att3[0] + t_att4[0];
	t_att[1] = t_att2[1] + t_att3[1] + t_att4[1];
	t_att[2] = t_att2[2] + t_att3[2] + t_att4[2];
	t_att[3] = t_att2[3] + t_att3[3] + t_att4[3];
	
    }
    else{
	U_att = pf_att(joint4,joint4g,Jacobian4,dlen,zeta,t_att);
    }

    double FP4[3] = {0,0,0};
    double FP3[3] = {0,0,0};
    double FP2[3] = {0,0,0};
    
    
    
    // Go through the list and get appropriate refpulsive torques
    while(head != NULL){
      
     	
      if(head->obs_type == 1){
	rho = rho1;
	eta = eta1;	 

	
#ifdef PF_DEBUG
	GetFloatingPoint(joint4,joint3,*head,FP_gui);	  
	printf("Floating point gui: %f %f %f\n",FP_gui[0],FP_gui[1],FP_gui[2]);
#endif

      }
      else if(head->obs_type == 2){
	rho = rho2;
	eta = eta2;
	
	if( isInCircle(joint4[0],joint4[1],head->x,head->y,head->radius_I,-padding_I) ){
	  
	    //printf("Inside circle\n");
	  
	  zeta = 100;
	  
	  U_att = pf_att(joint4,joint4g,Jacobian4,dlen,zeta,t_att);
	  
	  eta3 = 0;
	}
	
#ifdef PF_DEBUG
	GetFloatingPoint(joint4,joint3,*head,FP_gui);	  
	printf("Floating point gui: %f %f %f\n",FP_gui[0],FP_gui[1],FP_gui[2]);
#endif
      }
      else{
	rho = rho3;
	eta = eta3;  
	
      }
      
      
      U_rep2 += pf_rep(joint2,Jacobian2,rho,eta,*head,2,t_rep2);
      U_rep3 += pf_rep(joint3,Jacobian3,rho,eta,*head,3,t_rep3);
      U_rep4 += pf_rep(joint4,Jacobian4,rho,eta,*head,4,t_rep4);
      
      
      GetFloatingPoint(joint4,joint3,*head,FP4);
      
#ifdef PF_DEBUG	
      printf("floating point 4: %f %f %f\n",FP4[0],FP4[1],FP4[2]);
#endif
      
      GetFloatingPoint(joint3,joint2,*head,FP3);
      
#ifdef PF_DEBUG	
      printf("floating point 3: %f %f %f\n",FP3[0],FP3[1],FP3[2]);
#endif
      
      GetFloatingPoint(joint2,joint1,*head,FP2);
      
      
#ifdef PF_DEBUG	
      printf("floating point 2: %f %f %f\n",FP2[0],FP2[1],FP2[2]);
#endif
      
      
      Jacobianlf4   = Compute_Jacobian(q,joint1,joint2,joint3,joint4,FP4,4);
      Jacobianlf3   = Compute_Jacobian(q,joint1,joint2,joint3,joint4,FP3,3);
      Jacobianlf2   = Compute_Jacobian(q,joint1,joint2,joint3,joint4,FP2,2);
      
      U_rep4f      += pf_rep(FP4, Jacobianlf4, rho, eta, *head, 4, t_rep_f4);
      U_rep3f      += pf_rep(FP3, Jacobianlf3, rho, eta, *head, 3, t_rep_f3);
      U_rep2f      += pf_rep(FP2, Jacobianlf2, rho, eta, *head, 2, t_rep_f2);
      
      // Add the torques from temp to t_rep
      
      t_rep[0] += t_rep2[0] + t_rep3[0] + t_rep4[0] + t_rep_f2[0] + t_rep_f3[0] + t_rep_f4[0];
      t_rep[1] += t_rep2[1] + t_rep3[1] + t_rep4[1] + t_rep_f2[1] + t_rep_f3[1] + t_rep_f4[1];
      t_rep[2] += t_rep2[2] + t_rep3[2] + t_rep4[2] + t_rep_f2[2] + t_rep_f3[2] + t_rep_f4[2];
      t_rep[3] += t_rep2[3] + t_rep3[3] + t_rep4[3] + t_rep_f2[3] + t_rep_f3[3] + t_rep_f4[3];
      
      
      head = head->next;
      
      gsl_matrix_free(Jacobianlf2);
      gsl_matrix_free(Jacobianlf3);
      gsl_matrix_free(Jacobianlf4);
      
    }
    
    T_total[0]    = t_att[0] + t_rep[0];
    T_total[1]    = t_att[1] + t_rep[1];
    T_total[2]    = t_att[2] + t_rep[2];
    T_total[3]    = t_att[3] + t_rep[3];
    
    double U_total = U_att + U_rep2 + U_rep3 + U_rep4 + U_rep4f + U_rep3f + U_rep2f;
    
    
#ifdef PF_DEBUG	
    double U_rep = U_rep2 + U_rep3 + U_rep4 + U_rep4f + U_rep3f + U_rep2f;
    
    printf("Attractive potential   = %f\n",U_att);
    printf("Repulsive  potential   = %f\n",U_rep);
    printf("****Attractive Force = [%f %f %f]",F_TOTAL_ATT[0],F_TOTAL_ATT[1],F_TOTAL_ATT[2]);
    printf("****Repulsive Force = [%f %f %f]",F_TOTAL_REP[0],F_TOTAL_REP[1],F_TOTAL_REP[2]);
    // printf("Repulsive  potential 3 = %f\n",U_rep3);
    //printf("Repulsive  potential 4 = %f\n",U_rep4);
    //printf("Repulsive  potential 4f = %f\n",U_rep4f);
    //printf("Net torques             = %f\n",L2norm(T_total,4));
    printf("Finished objective computation\n");
#endif	
    
    
    
    gsl_matrix_free(Jacobian2);
    gsl_matrix_free(Jacobian3);
    gsl_matrix_free(Jacobian4);
    free(joint1);free(joint2);free(joint3);free(joint4);
   
    if(ex_pf){
      free(joint2g);free(joint3g);free(joint4g);
    }

    return U_total;
    
}

double pf_att(double *Oi,double *Og,gsl_matrix *J,
	    double dlen,double zeta,
	    double *t_att){

    double dist[3];
    double dist_norm;

    double F_att[3];

    double u_att;
    
    gsl_vector *F = gsl_vector_alloc(3);
    gsl_vector *T = gsl_vector_alloc(4);

    dist[0] = Oi[0] - Og[0];
    dist[1] = Oi[1] - Og[1];
    dist[2] = Oi[2] - Og[2];

    dist_norm = L2norm(dist,3);

   
    if( dist_norm <= dlen){

	u_att = 0.5*zeta*pow(L2norm(dist,3),2.0);

	double zeta_xy;

	if(zeta > 60)
	  zeta_xy = 60;
	else
	  zeta_xy = zeta;


	F_att[0] = -zeta_xy*( Oi[0] - Og[0] );
	F_att[1] = -zeta_xy*( Oi[1] - Og[1] );
	F_att[2] = -zeta*( Oi[2] - Og[2] );
	
      
    }
    else{

	u_att = dlen*zeta*L2norm(dist,3.0) - 0.5*zeta*dlen*dlen;

	F_att[0] = -zeta*dlen*( Oi[0] - Og[0] )/dist_norm;
	F_att[1] = -zeta*dlen*( Oi[1] - Og[1] )/dist_norm;
	F_att[2] = -zeta*dlen*( Oi[2] - Og[2] )/dist_norm;
	
    }

    F_TOTAL_ATT[0] = F_att[0];
    F_TOTAL_ATT[1] = F_att[1];
    F_TOTAL_ATT[2] = F_att[2];

    for(int i=0;i<3;i++)
	gsl_vector_set(F,i,F_att[i]);

    gsl_blas_dgemv (CblasTrans,1.0, J, F,0.0,T); 

    t_att[0] = gsl_vector_get(T,0);
    t_att[1] = gsl_vector_get(T,1);
    t_att[2] = gsl_vector_get(T,2);
    t_att[3] = gsl_vector_get(T,3);

#ifdef PF_DEBUG
    printf("Attractive potential = %f\n",u_att);
    printf("Attractive force = %f %f %f\n",F_att[0],F_att[1],F_att[2]);
    printf("torques = [%f %f %f %f]  Ncm\n",t_att[0],t_att[1],t_att[2],t_att[3]);
#endif


    gsl_vector_free(F);
    gsl_vector_free(T);


    return u_att;
   
}

double pf_rep(double *O,gsl_matrix *J,double roh_0,double eta,obstacle_t Obs,int num,double *t_rep){

    unsigned char obs_type = Obs.obs_type;

    double roh[4];

    double u_rep;

#ifdef PF_DEBUG
    printf("obs type = %d, link num = %d\n",obs_type,num);
#endif
	
    if(obs_type == 1)
	roh_cylinder(O,Obs,num,roh);

    else if(obs_type == 2)
	roh_annulus(O,Obs,num,roh);

    else if(obs_type == 3)
	roh_plane(O,Obs,roh);

#ifdef PF_DEBUG
    printf("obstacle type = %d, link = %d\n",obs_type,num);
    printf("roh distance = [%f %f %f], mag = %f cm\n",roh[0],roh[1],roh[2],roh[3]);
#endif


    if (roh[3]>roh_0){
	    
	t_rep[0]=0;
	t_rep[1]=0;
	t_rep[2]=0;
	t_rep[3]=0;
	    

	u_rep = 0;

	return u_rep; 
    }
      
    	
    double fac = eta*(1/roh[3]-1/roh_0)/(roh[3]*roh[3]);

    u_rep      = 0.5*eta*pow( (1/roh[3]-1/roh_0),2);
	
    gsl_vector * F = gsl_vector_alloc(3);
    gsl_vector * T = gsl_vector_alloc(4);

#ifdef PF_DEBUG
    printf("Repulsive potential = %f\n",u_rep);
    printf("Repuslive Force = [%f %f %f] N\n",fac*roh[0]/roh[3],fac*roh[1]/roh[3],fac*roh[2]/roh[3]);
#endif

    F_TOTAL_REP[0] += fac*roh[0]/roh[3];
    F_TOTAL_REP[1] += fac*roh[1]/roh[3];
    F_TOTAL_REP[2] += fac*roh[2]/roh[3];

	
    gsl_vector_set(F,0,fac*roh[0]/roh[3]);
    gsl_vector_set(F,1,fac*roh[1]/roh[3]);
    gsl_vector_set(F,2,fac*roh[2]/roh[3]);
    gsl_blas_dgemv (CblasTrans,1.0, J, F,0.0, T); 
	
    t_rep[0]=gsl_vector_get(T,0);
    t_rep[1]=gsl_vector_get(T,1);
    t_rep[2]=gsl_vector_get(T,2);
    t_rep[3]=gsl_vector_get(T,3);

#ifdef PF_DEBUG
    printf("forces = [%f %f %f], mag = %f N\n",fac*roh[0]/roh[3],fac*roh[1]/roh[3],fac*roh[2]/roh[3],roh[3]);
    printf("torques = [%f %f %f %f]  Ncm\n",t_rep[0],t_rep[1],t_rep[2],t_rep[3]);
#endif
	
    gsl_vector_free(F);
    gsl_vector_free(T);
	
    return u_rep;

}

void roh_plane(double *O, obstacle_t Obs,double *roh)
{
  // important: (roh[0],roh[1],roh[2]) is the distance vector and roh[3] is the magnitude of the distance
  
  double z = Obs.height;
  
  roh[0]=0;
  roh[1]=0;
  
  if (z<2)
    if(O[2]>=z)
	roh[2]=O[2];
    else
	roh[2]=(z-O[2]);
  else
    roh[2] = (O[2]-z);
  
  roh[3] = fabs(O[2]-z)/2;
  
}
  
void roh_cylinder(double *OA, obstacle_t obs,int num,double *roh){
  	  
  double OC[3];
  double BA[3];
  double CA[3];
  double CD[3];
  double OD[3];

  double padding = padding_cyl;

  obs.height = obs.height + h_padd_cyl;

  // reduce padding width for end effector
  if(num == 4)
    padding = padding_cyl;

  OC[0] = obs.x; 
  OC[1] = obs.y; 
  OC[2] = 0;

  if(OA[2] <= obs.height){
      OC[2] = OA[2];    //0;

      OD[0] = OA[0];
      OD[1] = OA[1];
      OD[2] = OA[2];
  }
  else{
      OC[2] = obs.height;

      OD[0] = OA[0];
      OD[1] = OA[1];
      OD[2] = obs.height;
  }

  CA[0] = OA[0] - OC[0];
  CA[1] = OA[1] - OC[1];
  CA[2] = OA[2] - OC[2];
  
  CD[0] = OD[0] - OC[0];
  CD[1] = OD[1] - OC[1];
  CD[2] = OD[2] - OC[2];


  if( ! isInCircle(OA[0],OA[1],OC[0],OC[1],obs.radius_O,padding)  ){
    
    BA[0] = CA[0] - (CD[0]/L2norm(CD,3) * (obs.radius_O + padding)  );
    BA[1] = CA[1] - (CD[1]/L2norm(CD,3) * (obs.radius_O + padding)  );
    BA[2] = CA[2] - (CD[2]/L2norm(CD,3) * (obs.radius_O + padding)  );

    roh[0] = BA[0];
    roh[1] = BA[1];
    roh[2] = BA[2];
    roh[3] = L2norm(BA,3)/2;

  }
  else{

    if(OA[2] > obs.height ){
      roh[0] = 0;
      roh[1] = 0;
      roh[2] = OA[2] - obs.height;
      roh[3] = fabs(roh[2]);
    }
    else{
      // collision (this is temporary)
  
      CA[0] = OA[0] - OC[0];
      CA[1] = OA[1] - OC[1];
      CA[2] = OA[2] - OC[2];
      
      roh[0] = CA[0];
      roh[1] = CA[1];
      roh[2] = CA[2];
      roh[3] = L2norm(CA,3);

    }
  }

}


void roh_annulus(double *OA, obstacle_t obs,int num,double *roh){
    
  double OC[3];
  double BA[3];
  double CA[3];
  double OD[3];  //Projection of point A on to the xy plane at the height of the cylinder
  double CD[3];

  OC[0] = obs.x; 
  OC[1] = obs.y; 
  OC[2] = 0;

  obs.height = obs.height + h_padd;

  if(num == 4)
     obs.height = obs.height + h_padd;
  
  if(OA[2] <= obs.height){
      OC[2] = OA[2];    //0;

      OD[0] = OA[0];
      OD[1] = OA[1];
      OD[2] = OA[2];
  }
  else{
      OC[2] = obs.height;

      OD[0] = OA[0];
      OD[1] = OA[1];
      OD[2] = obs.height;
  }

  CA[0] = OA[0] - OC[0];
  CA[1] = OA[1] - OC[1];
  CA[2] = OA[2] - OC[2];
  
  CD[0] = OD[0] - OC[0];
  CD[1] = OD[1] - OC[1];
  CD[2] = OD[2] - OC[2];
  
  if( ! isInCircle(OA[0],OA[1],OC[0],OC[1],obs.radius_O,padding_O)  ){
  
    BA[0] = CA[0] - (CD[0]/L2norm(CD,3) * (obs.radius_O + padding_O)  );
    BA[1] = CA[1] - (CD[1]/L2norm(CD,3) * (obs.radius_O + padding_O)  );
    BA[2] = CA[2] - (CD[2]/L2norm(CD,3) * (obs.radius_O + padding_O)  );
      	 
    roh[0] = BA[0];
    roh[1] = BA[1];
    roh[2] = BA[2];
    roh[3] = L2norm(BA,3)/2;

  }
  else{
    
    if( isInCircle(OA[0],OA[1],OC[0],OC[1],obs.radius_I,-padding_I) ){
      
      BA[0] = CA[0] - (CD[0]/L2norm(CD,3) * (obs.radius_I - padding_I)  );
      BA[1] = CA[1] - (CD[1]/L2norm(CD,3) * (obs.radius_I - padding_I)  );
      //BA[2] = CA[2] - (CD[2]/L2norm(CD,3) * (obs.radius_I - padding_I)  );
      BA[2] = 0;

      roh[0] = BA[0];
      roh[1] = BA[1];
      roh[2] = BA[2];
      roh[3] = L2norm(BA,3);

#ifdef PF_DEBUG
      printf("inside annulus\n");
#endif
    }
    else{
      
	if(OA[2] >= obs.height ){
	    roh[0] = 0;
	    roh[1] = 0;
	    roh[2] = (OA[2] - obs.height);
	    roh[3] = fabs(roh[2]);

#ifdef PF_DEBUG
	printf("Above annulus\n");
#endif
      }
      else{
	
	//BA[0] = -(CA[0] - (CD[0]/L2norm(CD,3) * (obs.radius_O + padding_O)  ));
	//BA[1] = -(CA[1] - (CD[1]/L2norm(CD,3) * (obs.radius_O + padding_O)  ));
	//BA[2] = -(CA[2] - (CD[2]/L2norm(CD,3) * (obs.radius_O + padding_O)  ));
	
	BA[0] = 0;
	BA[1] = 0;
	BA[2] = OA[2];	

	roh[0] = BA[0];
	roh[1] = BA[1];
	roh[2] = BA[2];
	roh[3] = L2norm(BA,3)/2;///10;

	
#ifdef PF_DEBUG
	printf("Inside annulus material\n");
#endif
	
      }
    }
  }
      
}
	

gsl_matrix * Compute_Jacobian(double *q,double *o1,double *o2,double *o3,double *o4,double *oc,
			      unsigned char num){

  double *A1      = mat_for_vx(q[0],q[1],q[2],q[3],1); 
  double *A2      = mat_for_vx(q[0],q[1],q[2],q[3],2);
  double *A3      = mat_for_vx(q[0],q[1],q[2],q[3],3);
  
  gsl_vector * Z0 = gsl_vector_alloc(3);
  gsl_vector * Z1 = gsl_vector_alloc(3);
  gsl_vector * Z2 = gsl_vector_alloc(3);
  gsl_vector * Z3 = gsl_vector_alloc(3);

  gsl_vector * O0 = gsl_vector_alloc(3);
  gsl_vector * O1 = gsl_vector_alloc(3);
  gsl_vector * O2 = gsl_vector_alloc(3);
  gsl_vector * O3 = gsl_vector_alloc(3);
  gsl_vector * O4 = gsl_vector_alloc(3);
  
  gsl_vector * On = gsl_vector_alloc(3);

  gsl_vector * J1 = gsl_vector_alloc(3);
  gsl_vector * J2 = gsl_vector_alloc(3);
  gsl_vector * J3 = gsl_vector_alloc(3);
  gsl_vector * J4 = gsl_vector_alloc(3);

  gsl_vector * temp = gsl_vector_alloc(3);

  gsl_matrix * J;
  
  gsl_vector_set(Z0,0,0);
  gsl_vector_set(Z0,1,0);
  gsl_vector_set(Z0,2,1);
  

  for(int i=0;i<3;i++){

    gsl_vector_set(Z1,i,A1[4*i+2]);
    gsl_vector_set(Z2,i,A2[4*i+2]);
    gsl_vector_set(Z3,i,A3[4*i+2]);

    gsl_vector_set(O0,i,0.0);
    gsl_vector_set(O1,i,o1[i]);
    gsl_vector_set(O2,i,o2[i]);
    gsl_vector_set(O3,i,o3[i]);
    gsl_vector_set(O4,i,o4[i]);
    gsl_vector_set(On,i,oc[i]);
  }

  vector_diff(On,O0,temp);
  cross_product(Z0,temp,J1);

  vector_diff(On,O1,temp);
  cross_product(Z1,temp,J2);

  vector_diff(On,O2,temp);
  cross_product(Z2,temp,J3);

  vector_diff(On,O3,temp);
  cross_product(Z3,temp,J4);

  J = GetMatrix(J1,J2,J3,J4,num);
  
  free(A1); free(A2); free(A3);
  gsl_vector_free(J1); gsl_vector_free(J2); gsl_vector_free(J3); gsl_vector_free(J4);
  gsl_vector_free(O0); gsl_vector_free(O1); gsl_vector_free(O2); gsl_vector_free(O3);
  gsl_vector_free(O4); gsl_vector_free(Z0); gsl_vector_free(Z1); gsl_vector_free(Z2);
  gsl_vector_free(Z3); gsl_vector_free(On); gsl_vector_free(temp);
  
  return J;
  
}

void cross_product(const gsl_vector *u, const gsl_vector *v, gsl_vector *product)
{
  double p1 = gsl_vector_get(u, 1)*gsl_vector_get(v, 2)
    - gsl_vector_get(u, 2)*gsl_vector_get(v, 1);
 
  double p2 = gsl_vector_get(u, 2)*gsl_vector_get(v, 0)
    - gsl_vector_get(u, 0)*gsl_vector_get(v, 2);
 
  double p3 = gsl_vector_get(u, 0)*gsl_vector_get(v, 1)
    - gsl_vector_get(u, 1)*gsl_vector_get(v, 0);
 
  gsl_vector_set(product, 0, p1);
  gsl_vector_set(product, 1, p2);
  gsl_vector_set(product, 2, p3);
}


void vector_diff(const gsl_vector *u,const gsl_vector *v,gsl_vector *w){

  double p1 = gsl_vector_get(u, 0) - gsl_vector_get(v, 0);
  double p2 = gsl_vector_get(u, 1) - gsl_vector_get(v, 1);
  double p3 = gsl_vector_get(u, 2) - gsl_vector_get(v, 2);
  

  gsl_vector_set(w,0,p1);
  gsl_vector_set(w,1,p2);
  gsl_vector_set(w,2,p3);
    

}
 
gsl_matrix * GetMatrix(const gsl_vector *J1,const gsl_vector *J2,
			const gsl_vector *J3,const gsl_vector *J4,int num){
  
  
  gsl_matrix * J = gsl_matrix_alloc (3,4);
  
  double v1,v2,v3,v4;
  

  for(int i=0;i<3;i++){
    
    v1 = gsl_vector_get(J1,i);
    v2 = gsl_vector_get(J2,i);
    v3 = gsl_vector_get(J3,i);
    v4 = gsl_vector_get(J4,i);

    gsl_matrix_set(J,i,0,v1);
    gsl_matrix_set(J,i,1,v2);
    gsl_matrix_set(J,i,2,v3);
    gsl_matrix_set(J,i,3,v4);
    
    
    if(num==3){
      gsl_matrix_set(J,i,3,0.0);
    }

    if(num==2){
      gsl_matrix_set(J,i,3,0);
      gsl_matrix_set(J,i,2,0);
    }
    
    if(num==1){
      gsl_matrix_set(J,i,3,0);
      gsl_matrix_set(J,i,2,0);
      gsl_matrix_set(J,i,1,0);
    }
    
  }
  

      
  return J;
}


double L2norm(double *x,int n){

    double mag = 0;

    for(int i=0;i<n;i++)
	mag += x[i]*x[i];

    mag = sqrt(mag);

    return mag;

}


bool isInCircle(double px, double py, double center_x, double center_y, double radius, double padding){
	//r^2 > (cx-x)^2 + (cx-x)^2 
	return pow(radius + padding, 2) > distSquared(px, py, center_x, center_y);
}

double getDistance(double x1, double y1, double x2, double y2){
	return sqrt(distSquared(x1, y1, x2, y2));
}

double distSquared(double x1, double y1, double x2, double y2){
	return pow((x1 - x2), 2) + pow((y1 - y2), 2);
}

double getError(double qi[4], double qf[4],double wgoal[3],unsigned char ex_pf){
    
    double mag = 0;

    if(!ex_pf){
        double * w_current = tip_pos(qi[0], qi[1], qi[2], qi[3], 4);
	
	double mag_error[3];
	
	mag_error[0] = w_current[0] - wgoal[0];
	mag_error[1] = w_current[1] - wgoal[1];
	mag_error[2] = w_current[2] - wgoal[2];
	
	mag = L2norm(mag_error,3);
	
	
    }
    else{
	double q_err[4];

	q_err[0] = qi[0] - qf[0];
	q_err[1] = qi[1] - qf[1];
	q_err[2] = qi[2] - qf[2];
	q_err[3] = qi[3] - qf[3];

	mag = L2norm(q_err,4);
	
    }


    return mag;
}

double fRand(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}


void gradientDescent(double q_start[4], double q_end[4],double wgoal[3], 
		     obstacle_t * obs,unsigned char start,unsigned char ex_pf,double time0){

    double alpha     = 0;

    double qi_prev[4];
    double torque[4];

    double error;
    int backNodes = 5;

    double precision = 0.7;

    if(ex_pf)
      precision = 1;

    double time = 0;	
    
    double err_history[20] = {0,1,2,3,4,5,6,7,8,9,
			      0,1,2,3,4,5,6,7,8,9};
                              	
    double mean1 = 0;
    double mean2 = 0;
    double mean3 = 0;
    double mean4 = 0;

    double diff1,diff2;

    double *qi = q_start;

    double eps = 0;

    bool doRandom = false;

    int count = 0;

    if(start)
	createList_waypoints(time, qi);

    
    error = getError(qi, q_end, wgoal,ex_pf);

    while (error > precision){

        count++;
	
	if(count > MAX_ITR){
	  printf("error = %f\n",error);
	  break;
	}
	
	if(ex_pf){
	    if(error>10)
		alpha = 1;
	    else if(error>6)
		alpha = 1;
	    else if(error>3)
		alpha = 1;
	    else if(error>2)
		alpha = 0.2;
	    else
		alpha = 0.1;
	}
	else{
	    if(error>10)
		alpha = 2;
	    else if(error>6)
		alpha = 1;
	    else if(error>3)
		alpha = 0.5;
	    else if(error>2)
		alpha = 0.2;
	    else
		alpha = 0.1;
	}
	//printf("alpha = %f\n",alpha);

	memcpy(qi_prev, qi, sizeof(qi_prev));

	// Get gradient of potential field
	pf_objfun(qi_prev, q_end,wgoal, obs, torque,ex_pf);

	// Get errpr between current tip position and final tip position
	error = getError(qi, q_end, wgoal, ex_pf);
			
	for(int j=19;j>=1;j--)
	    err_history[j] = err_history[j-1];

	err_history[0] = error;
	
	mean1 = 0;mean2 = 0;
	mean3 = 0;mean4 = 0;

	for(int j=0;j<5;j++){
	    mean1 += err_history[j];
	    mean2 += err_history[5+j];	
	    mean3 += err_history[10+j];	
	    mean4 += err_history[15+j];	
	    
	}

	mean1 = mean1/5;mean2 = mean2/5;
	mean3 = mean3/5;mean4 = mean4/5;
	
	//printf("means: %f,%f,%f,%f\n",mean1,mean2,mean3,mean4);

	diff1 = fabs(mean1-mean2);
	diff2 = fabs(mean3-mean4);
       
	if( error > 4 )
	    eps = 0.05;
	else if(error > 2)
	    eps = 0.013;
	else
	  eps = 0.004;
	    
	if( fabs(diff1-diff2)   <  eps ){
	   backNodes = 2;
	    doRandom = true;
	}
	    

	if(backNodes == 0){
	  doRandom = false;	
	}

	if(doRandom){

	    WayPoints_t *ptr;
	    ptr = getLastNode_waypoints();
	    unsigned char check = 1;
		  
	    int randm = 0;

	    while (check>0){

	      if(error > 5){
		    randm = 3;
		    if(ex_pf){
		      randm = 8;
		    }
	      }
		else if (error > 2)
		    randm = 2;
		else
		  randm = 1;

	      //printf("recomputing configuration\n");

		for (int i = 0; i < 4; i++){
		    qi[i] = ptr->servo[i] +   ( fRand(-randm, randm) );
		    if (qi[i]>max_servo[i])
			  qi[i] = max_servo[i];
			else if (qi[i]<min_servo[i])
			  qi[i] = min_servo[i];
		}
				
		check = cc_check(qi,obs);

		//check = 0;

	    }
		  
	    backNodes--;
	    
	    doRandom = false;	

	    
	    //printf("******************* random walk *******************\n");
		  

	}
	else{

	    unsigned char check = 1;

	    while(check > 0){
		
		for (int i = 0; i < 4; i++){
		    qi[i] = qi_prev[i] + alpha * torque[i]/L2norm(torque,4);
		    if (qi[i]>max_servo[i])
			  qi[i] = max_servo[i];
		    else if (qi[i]<min_servo[i])
			  qi[i] = min_servo[i];
		}
		
		check = cc_check(qi,obs);
		
		//printf("Collision check status = %d\n",check);
		
		if(check > 0){      
		    alpha = alpha - 0.1;
		    //printf("error = %f, reducing alpha to %f\n",getError(qi,q_end,wgoal,ex_pf),alpha);
		}
		
		if(alpha <= 0)
		    alpha = alpha;		
	    }
	    
	}
		
	qi[0] = gslu_math_mod2pi(qi[0] * DTOR) * RTOD;		
	qi[1] = gslu_math_mod2pi(qi[1] * DTOR) * RTOD;
	qi[2] = gslu_math_mod2pi(qi[2] * DTOR) * RTOD;
	qi[3] = gslu_math_mod2pi(qi[3] * DTOR) * RTOD;
  
		
	error = getError(qi, q_end, wgoal,ex_pf);
		
	//printf("count = %d, waypoint = %f %f %f %f\n",count,qi[0],qi[1],qi[2],qi[3]);
	
	addList_waypoints(time0 + time,qi);
	
	time = time + time_step;
		
	//printf("error = %f\n",error);
		
    }

    printf("error = %f\n",error);

    ERROR_PF = error;
	
}



unsigned char cc_check(double *q, obstacle_t *obs){

 
  unsigned char check = 0;

  for (int i=0;i<4;i++){
    if (q[i]>max_servo[i] || q[i]<min_servo[i]){
      check = 1;
      return check;
    }
  }

  double *joint1    = tip_pos(q[0],q[1],q[2],q[3],1);
  double *joint2    = tip_pos(q[0],q[1],q[2],q[3],2);
  double *joint3    = tip_pos(q[0],q[1],q[2],q[3],3);
  double *joint4    = tip_pos(q[0],q[1],q[2],q[3],4); // End effector

  obstacle_t *head   = obs;
 
  
  double FP2[3] = {0,0,0};
  double FP3[3] = {0,0,0};
  double FP4[3] = {0,0,0};

 
  check = 0;
	 
  // Go through the obstacle list and do collision checks
  while(head != NULL){
 

     GetFloatingPoint(joint4,joint3,*head,FP4);
     GetFloatingPoint(joint3,joint2,*head,FP3);
     GetFloatingPoint(joint2,joint1,*head,FP2);
     
      
    if(head->obs_type == 3){
      
      check += cc_check_floor(*head,joint1);
      check += cc_check_floor(*head,joint2);
      check += cc_check_floor(*head,joint3);
      check += cc_check_floor(*head,joint4);
      check += cc_check_floor(*head,FP2);
      check += cc_check_floor(*head,FP3);
      check += cc_check_floor(*head,FP4);

      if(check > 0){
	break;
      }
    }
    
    
    if(head->obs_type == 1){
      
      check += cc_check_cylinder(*head,joint1,1);
      check += cc_check_cylinder(*head,joint2,2);
      check += cc_check_cylinder(*head,joint3,3);
      check += cc_check_cylinder(*head,joint4,4);
      
      check += cc_check_cylinder(*head,FP2,2);
      check += cc_check_cylinder(*head,FP3,3);
      check += cc_check_cylinder(*head,FP4,4);
      

      if(check > 0){
	break;
	}
    }


    if(head->obs_type == 2){
      
      check += cc_check_annulus(*head,joint1,1);
      check += cc_check_annulus(*head,joint2,2);
      check += cc_check_annulus(*head,joint3,3);
      check += cc_check_annulus(*head,joint4,4);
      
      check += cc_check_annulus(*head,FP2,2);
      check += cc_check_annulus(*head,FP3,3);
      check += cc_check_annulus(*head,FP4,4);
      

      if(check > 0){
	break;
	}
    }
    

    head = head->next;  
  } 
  
  
#ifdef PF_DEBUG
  if (check > 0)
    printf("Collision for q1 = %lf,   q2 = %lf,   q3 = %lf,   q4 = %lf\n",q[0],q[1],q[2],q[3]);
#endif
  free(joint1);free(joint2);free(joint3);free(joint4);
  return check;
}


unsigned char cc_check_floor(obstacle_t obs,double *point){


  if(obs.obs_type != 3)
    return 0;

  //double x = point[0];
  //double y = point[1];
  double z = point[2];

  double floor_h = obs.height;

  unsigned char collision = 0;

  if(floor_h < 5){
    
    if(z < floor_h)
      collision = 1;
    
  }
  else{
    if(z > floor_h)
      collision = 1;

  }

#ifdef PF_DEBUG
  if(collision)
      printf("Collision on %f floor/table\n",obs.height);
#endif

  return collision;
  
}

unsigned char cc_check_cylinder(obstacle_t obs,double *point,int num){

  if(obs.obs_type != 1)
    return 0;



  double x = point[0];
  double y = point[1];
  double z = point[2];

  unsigned char collision = 0;

  double xc = obs.x;
  double yc = obs.y;
  double height = obs.height + h_padd_cyl;
  double radius = obs.radius_O;

  double padding;


  if(num == 4)
    padding = padding_cyl;
  else
    padding = padding_cyl;
  

  if(z <= height){
    if(isInCircle(x,y,xc,yc,radius,padding))
      collision = 1;
  }

#ifdef PF_DEBUG
  if(collision)
    printf("Collision on cylinder\n");
#endif

  
  return collision;
    
}

unsigned char cc_check_annulus(obstacle_t obs,double *point,int num){

  if(obs.obs_type != 2)
    return 0;

  double x = point[0];
  double y = point[1];
  double z = point[2];

  unsigned char collision = 0;

  double xc = obs.x;
  double yc = obs.y;
  double height = obs.height + h_padd;
  double radiusO = obs.radius_O;
  double radiusI = obs.radius_I;

  double padding;

  if(num == 4)
    padding = padding_O4;
  else
    padding = padding_O;
   

  if(z <= height){
    if( isInCircle(x,y,xc,yc,radiusO,padding) && !isInCircle(x,y,xc,yc,radiusI,-padding_I)  )
      collision = 1;
  }

#ifdef PF_DEBUG
  if(collision)
    printf("Collision on annulus\n");
#endif  

  return collision;
    
}


// List functions
void createList_waypoints(double time, double *servo){
	
  WayPoints_t *ptr = (WayPoints_t*)malloc(sizeof(WayPoints_t));
	
  ptr->time  = time;
  memcpy(ptr->servo,servo,4*sizeof(double));		
  ptr->prev  = NULL;
  ptr->next  = NULL;
  
  waypoints  = ptr;
}

void addList_waypoints(double time, double *servo){
	WayPoints_t *ptr = (WayPoints_t*)malloc(sizeof(WayPoints_t));

	WayPoints_t *temp = waypoints;

	ptr->time = time;
	memcpy(ptr->servo,servo,4*sizeof(double));		
	ptr->next = NULL;

	
	while(temp->next != NULL)
	  temp = temp->next;

	
	temp->next = (WayPoints_t*)malloc(sizeof(WayPoints_t));
	memcpy(temp->next,ptr,sizeof(WayPoints_t));
	temp->next->next = NULL;
	temp->next->prev = temp;

	free(ptr);
	
}

void delLastNode_waypoints(){

	WayPoints_t *ptr = waypoints;

	if (ptr == NULL)
		return;

	if (ptr->next == NULL){
		waypoints = NULL;
		return;
	}
		

	while (ptr->next->next != NULL){// see 2 pointers ahead so ptr = n-1 ptr
		ptr = ptr->next;
	}

	ptr->next = NULL;

}

WayPoints_t * getLastNode_waypoints(){

	WayPoints_t *ptr = waypoints;

	while (ptr->next != NULL)// see 2 pointers ahead
		ptr = ptr->next;

	return ptr;
}

//Plan trajectory
void Plan_Trajectory(int reverse){

    WayPoints_t* temp_wp    = waypoints;

    Trajectory_t *temp_traj = trajectory;
    Trajectory_t *ptr;
    
    double TF               = 0;

    
    if(reverse == 0){
      printf("Clearing trajectory list\n");
      
      if(temp_traj != NULL){
	while(temp_traj->next != NULL){
	  ptr = temp_traj->next;
	  free(temp_traj);
	  temp_traj = ptr;
	}
	free(temp_traj);
	trajectory = NULL;
	
	
      }
    }

    //printf("reverse status = %d\n",reverse);
    
    if(temp_wp == NULL){
	printf("No waypoints available\n");
    }
    else{

      if(reverse == 0){
	
	printf("Forward trajectory computation\n");


	while(temp_wp->next != NULL){
	  
	  //	    printf("time = %f, angle = %f\n",temp_wp->time,temp_wp->servo[0]);
	  
	  temp_traj = getSegmentTrajectory(temp_wp,temp_wp->next,TF,0);
	  
	  if(trajectory == NULL){
	    //printf("first segment\n");
	    trajectory = (Trajectory_t*)calloc(1,sizeof(Trajectory_t));
	    memcpy(trajectory,temp_traj,sizeof(Trajectory_t));
	    trajectory->next = NULL;
	  }
	  else{
	    ptr = trajectory;
	    
	    while(ptr->next!=NULL)
	      ptr= ptr->next;
	    
	    ptr->next = (Trajectory_t*) calloc(1,sizeof(Trajectory_t));
	    
	    memcpy(ptr->next,temp_traj,sizeof(Trajectory_t));
	    ptr->next->next = NULL;
	    
	    free(temp_traj);
	    //		printf("added segment\n");
	    
	  }
	  
	  temp_wp = temp_wp->next;
	  
	}
	
      }
      else{

	while(temp_wp->next != NULL)
	  temp_wp = temp_wp->next;
	
	TF = temp_wp->time;

	printf("Reverse trajectory\n");
	printf("Forward trajectory final time = %f\n",TF);

	double TF2 = 0;

	if(reverse == -2)
	  TF2 = TF;

	while(temp_wp->prev != NULL){

	  temp_traj = getSegmentTrajectory(temp_wp,temp_wp->prev,TF,TF2);
	  
	  if(trajectory == NULL){
	    //printf("first segment\n");
	    trajectory = (Trajectory_t*)calloc(1,sizeof(Trajectory_t));
	    memcpy(trajectory,temp_traj,sizeof(Trajectory_t));
	    trajectory->next = NULL;
	  }
	  else{
	    ptr = trajectory;
	    
	    while(ptr->next!=NULL)
	      ptr= ptr->next;
	    
	    ptr->next = (Trajectory_t*) calloc(1,sizeof(Trajectory_t));
	    
	    memcpy(ptr->next,temp_traj,sizeof(Trajectory_t));
	    ptr->next->next = NULL;
	    
	    free(temp_traj);
	    //printf("added reverse segment\n");
	    
	  }
	  
	  temp_wp = temp_wp->prev;
	}



      }

    }

    printf("Computed segments\n");

  

}


//Get servo angles for given segment and time
void populateAngles(double time, Trajectory_t* segment, double * q_t){

  q_t[0] =   segment->servo1_a[0]
           + segment->servo1_a[1]*time 
           + segment->servo1_a[2]*pow(time,2) 
           + segment->servo1_a[3]*pow(time,3);

  q_t[1] =   segment->servo2_a[0]
           + segment->servo2_a[1]*time 
           + segment->servo2_a[2]*pow(time,2) 
           + segment->servo2_a[3]*pow(time,3);

  q_t[2] =   segment->servo3_a[0] 
           + segment->servo3_a[1]*time 
           + segment->servo3_a[2]*pow(time,2) 
           + segment->servo3_a[3]*pow(time,3);

  q_t[3] =   segment->servo4_a[0] 
           + segment->servo4_a[1]*time 
           + segment->servo4_a[2]*pow(time,2) 
           + segment->servo4_a[3]*pow(time,3);
}


//Get trajectory segment
Trajectory_t *getSegmentTrajectory(WayPoints_t* startPoints, WayPoints_t* endPoints,double TF,double TF2){

    Trajectory_t *trajSegment = (Trajectory_t*)calloc(1,sizeof(Trajectory_t));
    double * qStart = startPoints->servo;
    double * qEnd   = endPoints->servo;
      
    trajSegment -> t0 = fabs(startPoints->time - TF) + TF2;
    trajSegment -> tf = fabs(endPoints->time   - TF) + TF2;

    //printf("t0 = %f start = %f %f %f %f\n",trajSegment ->t0,qStart[0],qStart[1],qStart[2],qStart[3]);
    //printf("tf = %f end = %f %f %f %f\n",trajSegment -> tf,qEnd[0],qEnd[1],qEnd[2],qEnd[3]);
    

    populateTrajectoryCoeff(qStart[0],qEnd[0],0,0,trajSegment -> t0, trajSegment -> tf,trajSegment->servo1_a);
    populateTrajectoryCoeff(qStart[1],qEnd[1],0,0,trajSegment -> t0, trajSegment -> tf,trajSegment->servo2_a);
    populateTrajectoryCoeff(qStart[2],qEnd[2],0,0,trajSegment -> t0, trajSegment -> tf,trajSegment->servo3_a);
    populateTrajectoryCoeff(qStart[3],qEnd[3],0,0,trajSegment -> t0, trajSegment -> tf,trajSegment->servo4_a);

    


    
    return trajSegment;
}

//Get trajectory coefficient
void populateTrajectoryCoeff(double q0, double qf, double v0,double vf, double t0, double tf,double * a){
  
  tf = tf - t0;
  t0 = 0;

  q0   = q0 * M_PI/180;
  qf   = qf * M_PI/180;

  a[0] = q0;
  a[1] = v0;
  a[2] = (3*(qf-q0)-(2*v0+vf)*(tf-t0))/(pow((tf-t0),2));
  a[3] = (2*(q0-qf)+(v0+vf)*(tf-t0))/(pow((tf-t0),3));

}


void ClearWayPoints(){

  WayPoints_t *ptr3,*ptr3_2;

  ptr3 = waypoints;

  if(ptr3 != NULL){
    
    while(ptr3->next != NULL){
      ptr3_2 = ptr3->next;
      free(ptr3);
      ptr3 = ptr3_2;
    }
 
    free(ptr3);
  
  }  
  
  waypoints = NULL;

  printf("Cleared waypoints\n");
}
