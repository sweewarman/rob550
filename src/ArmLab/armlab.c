
#include "gui.h"
#include "rexarm.h"
#include "PathPlanning.h"
#include "StateMachine.h"

extern gsl_matrix * conv_Mat;
extern gsl_matrix * inv_conv_Mat;
extern WayPoints_t * waypoints;
extern Trajectory_t *trajectory;


int main (int argc, char *argv[])
{

  srand(time(NULL));

    rob550_init (argc, argv);
    gui_state_t *gui_state = state_create ();
    rex_state_t *arm_state = calloc (1, sizeof(*arm_state));
    state_machine_t *auto_state = calloc (1, sizeof(*auto_state));

    getopt_t *gopt = getopt_create ();

    // Parse arguments from the command line, showing the help
    // screen if required
    gopt = getopt_create ();
    getopt_add_bool   (gopt,  'h', "help", 0, "Show help");
    getopt_add_bool   (gopt, 'i', "idle",  0, "Command all servos to idle");
    getopt_add_string (gopt, '\0', "url", "", "Camera URL");
    getopt_add_string (gopt, '\0', "status-channel", "ARM_STATUS", "LCM status channel");
    getopt_add_string (gopt, '\0', "command-channel", "ARM_COMMAND", "LCM command channel");

    gui_state->gopt = gopt;
   
    if (!getopt_parse (gopt, argc, argv, 1) || getopt_get_bool (gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (gopt);
        exit (EXIT_FAILURE);
    }




    // Set up the imagesource. This looks for a camera url specified on
    // the command line and, if none is found, enumerates a list of all
    // cameras imagesource can find and picks the first url it finds.
    if (strncmp (getopt_get_string (gopt, "url"), "", 1)) {
        gui_state->img_url = strdup (getopt_get_string (gopt, "url"));
        printf ("URL: %s\n", gui_state->img_url);
    }
    else {
        // No URL specified. Show all available and then use the first
        zarray_t *urls = image_source_enumerate ();
        printf ("Cameras:\n");
        for (int i = 0; i < zarray_size (urls); i++) {
            char *url;
            zarray_get (urls, i, &url);
            printf ("  %3d: %s\n", i, url);
        }

        if (0==zarray_size (urls)) {
            printf ("Found no cameras.\n");
            return -1;
        }

        zarray_get (urls, 0, &gui_state->img_url);
    }



    // Initialize this application as a remote display source. This allows
    // you to use remote displays to render your visualization. Also starts up
    // the animation thread, in which a render loop is run to update your display.
    vx_remote_display_source_t *cxn = vx_remote_display_source_create (&gui_state->vxapp);




    // Initialize a parameter gui
    gui_state->pg = pg_create ();




    // Setup buttons
    pg_add_buttons (gui_state->pg,
                    "but1",  "Anchor",
		    "but5",  "Calibrate",NULL);

    printf("5\n");

    pg_add_buttons (gui_state->pg,
		    "but6",  "Cylinder",
		    "but7",  "Annulus",
		    "but8",  "Clear",NULL);


    printf("6\n");

    pg_add_buttons (gui_state->pg,
		    "but9",  "Record",
		    "but10", "Plan",
		    "but11", "Play",
		    "but12", "Stop",
		    "but13", "PRM",NULL);

   
    printf("7\n");

    pg_add_check_boxes (gui_state->pg,
			"cb1", "Inverse",  0  ,
                        "cb2", "Elbow Up" , 1,
                        "cb3", "Reverse config", 0,
			"cb4", "Teach",0,
			"cb5", "Planner - PF",0,
			"cb7", "Planner - PRM",0,
			"cb6", "Ghost arm",0,
                        NULL);

    printf("8\n");

    // Add numerical entry boxes
    pg_add_double_boxes(gui_state->pg,"db1","Speed",0.05,
                                      "db2","Torque",0.35,
			              "db3","Play X",1.0,
			              "db4","Z",1.0,
		    	              "db5","Pitch",0.0,NULL);

    /*

    pg_add_double_boxes(gui_state->pg,"db1","Speed  ",0.05,NULL);
    pg_add_double_boxes(gui_state->pg,"db2","Torque ",0.35,NULL);
    pg_add_double_boxes(gui_state->pg,"db3"," Play X",1,NULL);
    pg_add_double_boxes(gui_state->pg,"db4","      Z",1,NULL);           
    pg_add_double_boxes(gui_state->pg,"db5","   Pitch",1.0,NULL);
			            
    */

    // Add sliders
    pg_add_double_slider (gui_state->pg, "sl1", "Servo 1", -180, 180, 0);
    pg_add_double_slider (gui_state->pg, "sl2", "Servo 2", -180, 180, 0);
    pg_add_double_slider (gui_state->pg, "sl3", "Servo 3", -180, 180, 0);
    pg_add_double_slider (gui_state->pg, "sl4", "Servo 4", -180, 180, 0);


    pg_add_buttons (gui_state->pg,
		    "but14", "Get cylinder",
		    "but15", "Get annulus",
		    "but16", "Create OBS",
		    "but17", "DMM", NULL);
		  

    // Initialize paramtere listeners for GUI
    parameter_listener_t *my_listener = calloc (1, sizeof(*my_listener));
    my_listener->impl = gui_state;
    my_listener->param_changed = my_param_changed;
    pg_add_listener (gui_state->pg, my_listener);

    // Setup rexarm 
    arm_state->running         = 1;
    arm_state->lcm             = lcm_create (NULL);
    arm_state->command_channel = getopt_get_string (gopt, "command-channel");
    arm_state->status_channel  = getopt_get_string (gopt, "status-channel");

    // Get current time 
    gettimeofday(&(gui_state->start_time), NULL);
    gettimeofday(&(arm_state->start_time), NULL);

    // Launch our worker threads
    pthread_create (&gui_state->animate_thread, NULL, animate_thread, gui_state);
    pthread_create (&arm_state->status_thread,  NULL, status_loop,    arm_state);
    pthread_create (&arm_state->command_thread, NULL, command_loop,   arm_state);
    pthread_create (&arm_state->command_thread, NULL, command_loop,   arm_state);
    pthread_create (&auto_state->state_machine_thread, NULL,run_state_machine,  gui_state);

    // This is the main loop
    rob550_gui_run (&gui_state->vxapp, gui_state->pg, 1024, 768);

    // Quit when GTK closes
    gui_state->running = 0;
    arm_state->running = 0;

    pthread_join (gui_state->animate_thread, NULL);
    
    // Probably not needed, given how this operates
    pthread_join (arm_state->status_thread, NULL);
    pthread_join (arm_state->command_thread, NULL);
    pthread_join (auto_state->state_machine_thread,NULL);

    // Cleanup
    free (my_listener);
    state_destroy (gui_state);
    free(auto_state);
    vx_remote_display_source_destroy (cxn);
    vx_global_destroy ();

    printf("Closing program\n");

    lcm_destroy (arm_state->lcm);
    free (arm_state);
    
    if(conv_Mat != NULL){
	free(conv_Mat);
	free(inv_conv_Mat);
    }
    
    WayPoints_t *temp1;
    Trajectory_t *temp2;

    while(waypoints){
	temp1 = waypoints->next;
	free(waypoints);
	waypoints = temp1;
    }

    while(trajectory){
	temp2 = trajectory->next;
	free(trajectory);
	trajectory = temp2;
    }
}

