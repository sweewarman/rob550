// PRM planner
#include "gui.h"
#include "PathPlanning.h"
#include "PRM.h"

extern double max_servo[4];
extern double min_servo[4];

const double seg_times[4]     = {0.2,0.3,0.5,0.7};//{0.4,0.7,0.9,1.2}; // in seconds
const double seg_distances[3] = {40,100,200}; // l2-norm of q0[4] and q1[4] in degrees 
const double n_check[4]       = {20,30,60,90};

const double z_limit = 3;
const double grid_p[5][3] = {{5.4,113,68.2},
			     {26.4,79.1,69.9},
			     {49.4,37.7,75.8},
			     {82,-1.3,46.9},
			     {0,90,0}};

// globals
double *distArray;
int *lookedList;
int *path;
int lookedListInx;
int pathInx;
double DOUBLE_MAX = 10000000000;
graph_t *road_map = NULL;

extern int closest_qi;


//#define SEARCH_DEBUG

unsigned char PRM_Planner(double *qi,double *qf,double *wf,obstacle_t *obs){

 
  int closest_qf;

  int *path;
  printf("start find_end_point\n");   
  closest_qf = find_end_point(road_map,wf);
  printf("finished find_end_point\n");
 
  path       = getPath(closest_qi,closest_qf,road_map);

  //path       = getPath(closest_qi,closest_qf,road_map);

  if(pathInx == -1){
    printf("No path to goal\n");
    return 0;
  }

  printf("PathInx = %d\n",pathInx);

  printf("start : %f %f %f %f\n",qi[0],qi[1],qi[2],qi[3]);

  printf("closest to start %f %f %f %f\n",road_map->nodes[closest_qi].q[0],
	 road_map->nodes[closest_qi].q[1],
	 road_map->nodes[closest_qi].q[2],
	 road_map->nodes[closest_qi].q[3]);

  printf("closest to end %f %f %f %f\n",road_map->nodes[closest_qf].q[0],
	 road_map->nodes[closest_qf].q[1],
	 road_map->nodes[closest_qf].q[2],
	 road_map->nodes[closest_qf].q[3]);




  for(int i=0;i<pathInx;i++){
    
    printf("waypoint %d: %d - %f %f %f %f\n",i,road_map->nodes[path[i]].id,
	   road_map->nodes[path[i]].q[0],
	   road_map->nodes[path[i]].q[1],
	   road_map->nodes[path[i]].q[2],
	   road_map->nodes[path[i]].q[3]);
    
  }
  
  printf("\nend : %f %f %f %f\n",qf[0],qf[1],qf[2],qf[3]);

  GetPRMWaypoints(qi,qf,wf,path,road_map,obs);
  
  return 1;

}

void GetPRMWaypoints(double *qi,double *qf,double *wf,int *prm_wp,graph_t *map,obstacle_t *obs){

    int    node_id  = 0;
    int j           = 0;

    double dist     = 0;
    double time     = 0;
    double q1[4]    = {0,0,0,0};
    double q2[4]    = {0,0,0,0};

    double *temp;
   

    if(pathInx == -1){
	printf("No paths from qi to qf\n");
	return;
    }

    //Create the head of the waypoint
    createList_waypoints(time, qi);
    
   
    for(int i=0; i< pathInx;i++){
	

	node_id = prm_wp[i];

	if(i==0){
  
	    memcpy(q1,qi,4*sizeof(double));

	    temp   = map->nodes[ node_id ].q; 
	    memcpy(q2,temp,4*sizeof(double));
	}
	else{

	    temp   = map->nodes[ node_id-1 ].q; 
	    memcpy(q1,temp,4*sizeof(double));

	    temp   = map->nodes[ node_id ].q; 
	    memcpy(q2,temp,4*sizeof(double));
	}

	dist = L2norm_diff4( q1,q2 );

	while( dist > seg_distances[j] && j < 3 )
	    j = j+1;


	time = time + seg_times[j];
	    	    
	addList_waypoints(time,q2);
	
	//printf("Added time = %f\n",time);
     
    }
   
    node_id = prm_wp[pathInx-1];
    
    // Use the potential field planner to get from n-1 th to n th node
    temp   = map->nodes[ node_id ].q;
    memcpy(q1,temp,4*sizeof(double));
    
    //temp   = map->nodes[ pathInx ].q; 
    //memcpy(q1,temp,4*sizeof(double));
 
    printf("Init conditions = %f %f %f %f\n",q1[0],q1[1],q1[2],q1[3]);
    
    gradientDescent(q1,qf,wf,obs,0,0,time);
    
    return;
}


int find_end_point(graph_t *graph,double goal[3]){
  int id = 0;
  double *tip4;
  double *tip3;
  double diff[3];
  double max = 1e10;
  double dist;
  double phi; // orientation of tip wrt. horizontal
  for (int i=0;i<graph->n_nodes;i++){
    tip4 = tip_pos(graph->nodes[i].q[0],graph->nodes[i].q[1],graph->nodes[i].q[2],graph->nodes[i].q[3],4);
    tip3 = tip_pos(graph->nodes[i].q[0],graph->nodes[i].q[1],graph->nodes[i].q[2],graph->nodes[i].q[3],3);
    diff[0] = tip4[0]-goal[0];
    diff[1] = tip4[1]-goal[1];
    diff[2] = tip4[2]-goal[2];    
    dist = L2norm(diff,3);
    phi = fabs(graph->nodes[i].q[1]+graph->nodes[i].q[2]+graph->nodes[i].q[3]-90);
    if (dist<max && tip3[2]>8 && tip4[2]>5 && phi>50 && phi<140){
      max = dist;
      id = graph->nodes[i].id;
    }
	free(tip4);
	free(tip3);
  }
  return id;
}


int find_closest_point(graph_t *graph,double *q,obstacle_t *obs){
  double max = 1e10;
  int closest_node_id = 0;
  double dist;
  for (int i=0;i<graph->n_nodes;i++){
    dist = L2norm_diff4(q,graph->nodes[i].q);
    if (dist<max && (seg_check(q,graph->nodes[i].q,obs)==1)){
      max = dist;
      closest_node_id = graph->nodes[i].id;
    }
  }
  return closest_node_id;
}






int seg_check(double *q1, double *q2, obstacle_t *obs){
  int check = 1;
  double a_coeff[4];
  double q_check[4];
  double dist = L2norm_diff4(q1,q2);
  double tf = 0,q0=0,qf=0;
  int n=100;
  if (dist<=seg_distances[0]){
    tf = seg_times[0];
    n = n_check[0];
  }
  else if (dist > seg_distances[0] && dist <= seg_distances[1]){
    tf = seg_times[1];
    n = n_check[1];
  }
  else if (dist > seg_distances[1] && dist <= seg_distances[2]){
    tf = seg_times[2];
    n = n_check[2];
  }
  else if (dist > seg_distances[2]){
    tf = seg_times[3];
    n = n_check[3];
  }
  double times[n];
  for (int i=0; i<n; i++)
    times[i]=(i+1)*tf/((double)n+1);
  //q0   = q0 * M_PI/180;
  //qf   = qf * M_PI/180;
  // set v0 and vf always to zero!
  for (int j=0; j<n; j++){
    for (int i=0; i<4; i++){
      q0 = q1[i];
      qf = q2[i];	
      a_coeff[0] = q0;
      a_coeff[1] = 0;
      a_coeff[2] = (3*(qf-q0))/(pow(tf,2));
      a_coeff[3] = (2*(q0-qf))/(pow(tf,3));
      q_check[i] = a_coeff[0]+a_coeff[2]*pow(times[j],2)+a_coeff[3]*pow(times[j],3);
    }
    if(cc_check(q_check,obs)>0){
      check = 0;
      return check;
    }
  }
  return check;
}




void get_edges(graph_t * graph, obstacle_t *obs){
  int n = graph->n_nodes;
  int k = (int)k_edge;
  double dd[k_edge]; // contains the k closest distances (smallest costs)
  double max_d;
  int max_d_id;
  int n_id[k_edge]; // contains the ids of the k closest nodes
  double *q;
  //  int counter = 0;
  int edge_counter = 0;

  printf("n=%d\n",n);  

  for (int i=0;i<n;i++){
    q = graph->nodes[i].q;
    max_d = 1e10;
    for (int j=0;j<k;j++){
      // initialize
      dd[j]=1e10; 
      n_id[j] = 0;
    }
    //printf("%f  %f  %f  %f\n",q[0],q[1],q[2],q[3]);
    max_d_id = max_array(dd,k, &max_d);
    for (int j=0; j<n; j++){
      if (L2norm_diff4(q,graph->nodes[j].q)<max_d && (j!=i) && (seg_check(q,graph->nodes[j].q,obs)==1)){
	max_d_id = max_array(dd,k, &max_d);   
	dd[max_d_id] = L2norm_diff4(q,graph->nodes[j].q);   
	n_id[max_d_id] = j;
      }
    }
    for (int j=0;j<k;j++){
      if(edge_already_exists(graph,edge_counter,i,n_id[j])==0){
	graph->edges[edge_counter].id = edge_counter;
	graph->edges[edge_counter].p1 = i;
	graph->edges[edge_counter].p2 = n_id[j];
	graph->edges[edge_counter].cost = L2norm_diff4(q,graph->nodes[n_id[j]].q);  
	add_edge_id(&(graph->nodes[i]),edge_counter);
	add_edge_id(&(graph->nodes[n_id[j]]),edge_counter);
	edge_counter++;
      }
    }
  }
  graph->n_edges = edge_counter;
}

void add_edge_id(node_t *node, int edge_id){
  node->edge_ids[node->n_edge_ids] = edge_id;
  node->n_edge_ids++;
}

int edge_already_exists(graph_t * graph, int edge_counter, int p1, int p2){ 
  int check = 0; 
  for (int i=0;i<edge_counter;i++){
    if( (graph->edges[i].p1==p1 && graph->edges[i].p2 == p2) || (graph->edges[i].p1==p2 && graph->edges[i].p2 == p1) ){
      check = 1;
      //printf("caught %d-%d already existed --- %d-%d\n",p1,p2,graph->edges[i].p1,graph->edges[i].p2);
      break;
    }
  }
  return check;
}

void random_points(graph_t * graph, obstacle_t *obs, int add_points){
  int n = add_points + random_nodes;
  int n_grid = graph->n_nodes;
  double *joint3,*joint4;
  double j3,j4;
  double q_rand[4];
  unsigned char check; 
  for(int i=0;i<n;i++){
    check = 1;
    j3 = 30;
    j4 = 30;  
    while(check>0 || j3<z_limit || j4<z_limit){
      for(int j=0;j<4;j++){
	    q_rand[j] = fRand(min_servo[j],max_servo[j]);
      }
      joint3    = tip_pos(q_rand[0],q_rand[1],q_rand[2],q_rand[3],3);
      joint4    = tip_pos(q_rand[0],q_rand[1],q_rand[2],q_rand[3],4);
      j3 = joint3[2];
      j4 = joint4[2];
      free(joint3);
      free(joint4); 
      check = cc_check(q_rand,obs);      
    }
    for(int j=0;j<4;j++)
      graph->nodes[n_grid+i].q[j] = q_rand[j];
    graph->n_nodes = n_grid+1+i;
    graph->nodes[n_grid+i].id = n_grid+i;
  }
}

int graph_grid_points(graph_t * graph, obstacle_t *obs){
// this function works for 4 joints
  //int n=4;
  int m = (int)grid_pp_joint;
  double *joint3,*joint4;
  double q_grid[4];
  //unsigned char check;
  double range1 = max_servo[0]-min_servo[0];
  double range2 = max_servo[1]-min_servo[1];
  double range3 = max_servo[2]-min_servo[2];
  double range4 = max_servo[3]-min_servo[3];
  int node_count = 0;
  int add_points = 0;
  if (m>1){
    while (node_count<pow(m,4.0))
      for(int i = 0;i<m;i++){
	for(int j = 0;j<m;j++){
	  for(int v = 0;v<m;v++){
	    for(int p = 0;p<m;p++){
	      q_grid[0] = min_servo[0] + (double) i*range1/(m-1);
	      q_grid[1] = min_servo[1] + (double) j*range2/(m-1);
	      q_grid[2] = min_servo[2] + (double) v*range3/(m-1);
	      q_grid[3] = min_servo[3] + (double) p*range4/(m-1);
	      joint3    = tip_pos(q_grid[0],q_grid[1],q_grid[2],q_grid[3],3);
	      joint4    = tip_pos(q_grid[0],q_grid[1],q_grid[2],q_grid[3],4); 
	      if (cc_check(q_grid,obs)==0 && joint3[2]>z_limit && joint4[2]>z_limit){
		graph->nodes[node_count-add_points].q[0] = q_grid[0];
		graph->nodes[node_count-add_points].q[1] = q_grid[1];
		graph->nodes[node_count-add_points].q[2] = q_grid[2];
		graph->nodes[node_count-add_points].q[3] = q_grid[3];
		graph->nodes[node_count-add_points].id = node_count-add_points;
		graph->n_nodes = 1 + node_count-add_points; 
	      }
	      else
		add_points ++;
	      node_count++;
	      free(joint3);
	      free(joint4);
	    }
	  }
	}
      }
  }
  else if (m==-1)
    {
      int mm = (int)grid_pp_circle;
      for (int i=0; i<5;i++){
	for (int j=0; j<mm;j++){
	  q_grid[0] = -178.0 + j*356.0/(mm-1);
	  q_grid[1] = grid_p[i][0];
	  q_grid[2] = grid_p[i][1];
	  q_grid[3] = grid_p[i][2];
	  joint3    = tip_pos(q_grid[0],q_grid[1],q_grid[2],q_grid[3],3);
	  joint4    = tip_pos(q_grid[0],q_grid[1],q_grid[2],q_grid[3],4); 
	  if (cc_check(q_grid,obs)==0 && joint3[2]>z_limit && joint4[2]>z_limit){
	    graph->nodes[node_count-add_points].q[0] = q_grid[0];
	    graph->nodes[node_count-add_points].q[1] = q_grid[1];
	    graph->nodes[node_count-add_points].q[2] = q_grid[2];
	    graph->nodes[node_count-add_points].q[3] = q_grid[3];
	    graph->nodes[node_count-add_points].id = node_count-add_points;
	    graph->n_nodes = 1 + node_count-add_points; 
	  }
	  else
	    add_points ++;
	  free(joint3);
	  free(joint4);
	  node_count++;
	  q_grid[1] = -grid_p[i][0];
	  q_grid[2] = -grid_p[i][1];
	  q_grid[3] = -grid_p[i][2];
	  joint3    = tip_pos(q_grid[0],q_grid[1],q_grid[2],q_grid[3],3);
	  joint4    = tip_pos(q_grid[0],q_grid[1],q_grid[2],q_grid[3],4); 
	  if (cc_check(q_grid,obs)==0 && joint3[2]>z_limit && joint4[2]>z_limit){
	    graph->nodes[node_count-add_points].q[0] = q_grid[0];
	    graph->nodes[node_count-add_points].q[1] = q_grid[1];
	    graph->nodes[node_count-add_points].q[2] = q_grid[2];
	    graph->nodes[node_count-add_points].q[3] = q_grid[3];
	    graph->nodes[node_count-add_points].id = node_count-add_points;
	    graph->n_nodes = 1 + node_count-add_points; 
	  }
	  else
	    add_points ++;
	  free(joint3);
	  free(joint4);
	  node_count++;
	}
      }
    }
  printf("%d points out of %d grid points have collisions  (%f)\n",add_points,node_count,pow(m,4));
  return add_points;  
}

graph_t * graph_create (){
  int n_nodes;
  if(grid_pp_joint>1)
    n_nodes = random_nodes + pow(grid_pp_joint,4.0);
  else if (grid_pp_joint==-1)
	n_nodes = random_nodes + (int)grid_pp_circle*10;
  else
    n_nodes = random_nodes;
  //n_nodes++; // for adding fixed home position to the graph
  int k = n_nodes*k_edge;
  graph_t *graph = calloc (1, sizeof(graph_t));
  graph->nodes = calloc (n_nodes, sizeof(node_t));
  graph->edges = calloc (k, sizeof(edge_t));
  printf("allocated %d nodes and %d edges\n",n_nodes,k);
  k = (int) 5*k_edge;
  for (int i=0;i<n_nodes;i++){
    graph->nodes[i].edge_ids = calloc(k,sizeof(int));
    graph->nodes[i].n_edge_ids = 0;
  }
  graph->n_nodes = 0;//initialize//n_nodes;
  return graph;
}


int max_array(double *d,int k, double *max_d){
  int id = 0;
  double  max = -1;
  for(int i = 0 ; i<k ; i++){
    if ( d[i]>max ){
      id = i;
      max = d[i];
    }
  }
  *max_d = max;
  return id;
}

void print_graph(graph_t *graph)
{
  for (int i=0; i < graph->n_nodes;i++){
    printf("\n ----- node %d -----\n",i);
    for (int j=0;j<graph->nodes[i].n_edge_ids;j++)
      printf("edge %d: %d to %d, cost: %f\n",j,graph->edges[graph->nodes[i].edge_ids[j]].p1,graph->edges[graph->nodes[i].edge_ids[j]].p2,graph->edges[graph->nodes[i].edge_ids[j]].cost);
  }
}

/*double fRand(double fMin, double fMax){
  double f = (double) rand()/RAND_MAX;
  return fMin + f*(fMax-fMin);
}*/ // same as John's function DONT FORGET TO SEED RAND()

double L2norm_diff4(double q0[4],double q1[4]){
    double mag = 0;
	double x[4];
	x[0]=q0[0]-q1[0];
	x[1]=q0[1]-q1[1];
	x[2]=q0[2]-q1[2];
	x[3]=q0[3]-q1[3];
    for(int i=0;i<4;i++)
	mag += x[i]*x[i];
    mag = sqrt(mag);
    return mag;
}







void addLookedList(int nodeNumber){

    lookedList[lookedListInx] = nodeNumber;

    lookedListInx++;

}



void updateDistArr(int nodeId, double newRoutDistance){

    distArray[nodeId] = newRoutDistance;

}



bool lookedList_contains(int nodeId){



    for (int i = 0; i < lookedListInx; i++)

	if (lookedList[i] == nodeId)

	    return true;



    return false;

}





int getOtherNodeID(int nodeID, edge_t *edge){

    if (edge->p1 == nodeID)

	return edge->p2;

    else

	return edge->p1;



}



void populateDistArray(graph_t *graph, int source){





    addLookedList(source);

    updateDistArr(source, 0);

    int count = 0;

    while (lookedListInx < graph->n_nodes){

	//for(int n = 0;n < graph->n_nodes; n++){

	double minDist = DOUBLE_MAX;

	int minNodeID_dest = -1;

	int minNodeID_start = -1;



	for (int i = 0; i < lookedListInx; i++)

	{



	    int startNodeId = lookedList[i];

	    node_t *startNode = &(graph->nodes[startNodeId]);



	    if (startNode->n_explored_edge == startNode->n_edge_ids){

		//printf("*********skipping %d***********\n",startNodeId);

		continue;

	    }

	    for (int j = 0; j < startNode->n_edge_ids; j++){



		int edge_id = startNode->edge_ids[j];

		edge_t *edge = &(graph->edges[edge_id]);//graph->(edges + edge_id);



		int newRoutDist = distArray[startNodeId] + edge->cost;



		double destNodeID = getOtherNodeID(startNodeId, edge);



		if (!lookedList_contains(destNodeID)){



		    if (minDist > newRoutDist){

			minDist = newRoutDist;

			updateDistArr(destNodeID, newRoutDist);minNodeID_dest = destNodeID;

			minNodeID_start = startNodeId;

		    }



		}



	    }// edge for loop



	}// list for loop



	if (minNodeID_dest != -1){



	    addLookedList(minNodeID_dest);



	    node_t *destNode = &(graph->nodes[minNodeID_dest]);//graph->(nodes + destNodeID);

	    destNode->prevNodeId = minNodeID_start;



	    node_t *startNode = &(graph->nodes[minNodeID_start]);

	    startNode->n_explored_edge = startNode->n_explored_edge + 1; // can u do this? check


#ifdef SEARCH_DEBUG
	    printf("%d --> %d\n", minNodeID_start, minNodeID_dest);
#endif


	}

	count++;

	if (count > graph->n_nodes){

#ifdef SEARCH_DEBUG
	    printf("*********Unconnected Nodes**********\n");
#endif

	    break;

	}

    }// end of while loop



}





void getPath_rec(int sourceID, int dest, graph_t *graph){

    node_t *dest_node = &(graph->nodes[dest]);
	
	if (dest_node->prevNodeId == -1){

		printf("****%d is not connected****\n", dest);

		pathInx = -1;

		return;

    }

    if (dest_node->prevNodeId != sourceID)
		getPath_rec(sourceID, dest_node->prevNodeId, graph);

    //printf("%d\n",dest_node->prevNodeId);

    path[pathInx] = dest_node->prevNodeId;

    pathInx++;

}







//, int node_f

int * getPath(int node_s, int node_f, graph_t * graph){


	pathInx = 0;

	path = (int *)malloc(graph->n_nodes * sizeof(int));

	getPath_rec(node_s, node_f, graph);

	if (pathInx == -1){

		printf("*****NO SOLUTION******\n");

		return path;

	}

	path[pathInx] = node_f;

	pathInx++;

	return path;
}





void Dijkstra(graph_t *graph, int node_s){

  lookedListInx = 0;
  
  distArray = (double *)malloc(graph->n_nodes * sizeof(double));
  
  lookedList = (int *)malloc(graph->n_nodes * sizeof(int));
  
  for (int i = 0; i < graph->n_nodes; i++)
    (graph->nodes+i )->prevNodeId = -1;
  
  populateDistArray(graph, node_s);

}

