#ifndef PRM
#define PRM

#define grid_pp_circle 20 // "grid points per circle" --> grid points = (grid_pp_circle)*4
#define grid_pp_joint -1 // "grid points per joint" --> grid points = (grid_pp_joint)^4
#define random_nodes 400 // number of random sampling points
#define k_edge 4 // number of edges per node

// has to be defined as EXTERN in PRM.c!!!!!!!
typedef struct graph graph_t;
typedef struct node node_t;
typedef struct edge edge_t;

struct node {
    double q[4];     // joint angles 
    int id;          // id of node
    int *edge_ids;   // 1D array of ids of edges that originating from this node
    int n_edge_ids;
    int n_explored_edge;   // I am adding this extra
    int prevNodeId;
};


struct edge {
    int p1;    // id of start-node
    int p2;      // id of end-node
    double cost;  // edge cost/distance
    double time;  // time to travel this edge
    int id;       // id of edge
};

struct graph {
    node_t *nodes;  // 1D array of nodes
    int n_nodes;    // number of nodes
    int n_edges;    // number of edges
    edge_t *edges;  // 1D array of edges
};

int find_end_point(graph_t *graph,double goal[3]);

double L2norm_diff4(double q0[4], double q1[4]);

graph_t * graph_create ();

int graph_grid_points(graph_t * graph, obstacle_t *obs);

void random_points(graph_t * graph, obstacle_t *obs, int add_points);

void get_edges(graph_t * graph, obstacle_t *obs);

int max_array(double *d,int k, double *max_d);

int edge_already_exists(graph_t * graph, int edge_counter, int p1, int p2); // checks if edge already exists

void add_edge_id(node_t *node, int edge_id);

void print_graph(graph_t *graph);

int seg_check(double *q1, double *q2, obstacle_t *obs);

int find_closest_point(graph_t *graph,double *q,obstacle_t *obs);


// John
void addLookedList(int nodeNumber);

void updateDistArr(int nodeId, double newRoutDistance);

bool lookedList_contains(int nodeId);

int getOtherNodeID(int nodeID, edge_t *edge);

void populateDistArray(graph_t *graph,int source);

int* getPath(int sourceID,int dest,graph_t *graph);

void Dijkstra(graph_t *graph, int node_s);

void GetPRMWaypoints(double *qi,double *qf,double *wf,int *prm_wp,graph_t *map,obstacle_t *obs);

unsigned char PRM_Planner(double *qi,double *qf,double *wf,obstacle_t *obs);

#endif
