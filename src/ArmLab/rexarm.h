#ifndef REX_H
#define REX_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <sys/select.h>
#include <sys/time.h>
#include <pthread.h>

#include <lcm/lcm.h>
#include "lcmtypes/dynamixel_command_list_t.h"
#include "lcmtypes/dynamixel_command_t.h"
#include "lcmtypes/dynamixel_status_list_t.h"
#include "lcmtypes/dynamixel_status_t.h"

#include "common/getopt.h"
#include "common/timestamp.h"
#include "math/math_util.h"

#include "gui.h"

#define NUM_SERVOS 4
// Define max angles for each servo in radians

#define base_x 0.05
#define base_z 0.07
#define joint 0.03
#define link 0.10
#define effector 0.10

typedef struct REXstate rex_state_t;

struct REXstate
{
  getopt_t *gopt;

  unsigned char running;

  // LCM
  lcm_t *lcm;
  const char *command_channel;
  const char *status_channel;
    
  struct timeval start_time;
  
  pthread_t status_thread;
  pthread_t command_thread;
};


void status_handler (const lcm_recv_buf_t *rbuf,
                const char *channel,
                const dynamixel_status_list_t *msg,
                void *user);

void * status_loop (void *data);

void * command_loop (void *user);

void rexarm_clamp(double qi[4],double qo[4]);

bool isPointInWorkspace(double x,double y,double z);

vx_object_t * rexarm_vxo_arm(const int errors[4],const double angles[4],const float arm_color[4],float alpha);

gsl_matrix * H(double theta, double d, double a, double alpha); // computes homogeneous trafo matrix

double * tip_pos(double theta1, double theta2, double theta3, double theta4, int i); // forward kinematics

double * mat_for_vx(double theta1, double theta2, double theta3, double theta4, int i);

double * inv_kin(double xg, double yg, double zg, double phi, int pos, double *prev_servo);

unsigned char PlayTrajectory(void);

#endif
