
#include "StateMachine.h"
#include "gui.h"
#include "rexarm.h"
#include "PathPlanning.h"

#include "PRM.h"

extern pthread_mutex_t fdb_mutex;
extern pthread_mutex_t servo_mutex;
extern pthread_mutex_t DMM_mutex;

extern obstacle_t *OBS;

extern gui_cmds_t user_cmds;
extern gui_cmds_t servo_fdb;

extern double GOAL_ALT;

extern unsigned char PLAY;

extern pthread_mutex_t play_mutex;

extern double ERROR_PF; 
extern graph_t *road_map;

extern int closest_qi;


#define PAUSE_TIME 5.0

void * run_state_machine(void* data){
  
  double fdb_servo[4];
  unsigned char state    = 0;
  unsigned char target   = 0;
  unsigned char home     = 0;
  double targetPos[3]    = {0,0,0};
  
  double q_rend[4]       = {0.0,90.0,0.0,0.0};
  
  double q_diff[4];

  double time = 0;

  unsigned play = 1;
  
  printf("Starting DMM state machine function\n");
  
  struct timeval cur_time;
  static struct timeval rec_start_time;

  unsigned char start_rec = 0;

  while(1){
      
      pthread_mutex_lock(&DMM_mutex);
      gui_state_t *gui_state = data; 
      pthread_mutex_unlock(&DMM_mutex);
        

      //printf("DMM state = %d\n",gui_state->DMM_STATE);
    
      if(gui_state -> DMM_STATE){
	  
	for(int i=0;i<4;i++){
	  pthread_mutex_lock(&fdb_mutex);
	  fdb_servo[i] = servo_fdb.servo[i];
	  pthread_mutex_unlock(&fdb_mutex);
	}
	  
	q_diff[0] = fdb_servo[0] - q_rend[0];
	q_diff[1] = fdb_servo[1] - q_rend[1];
	q_diff[2] = fdb_servo[2] - q_rend[2];
	q_diff[3] = fdb_servo[3] - q_rend[3];
	
	if(L2norm(q_diff,4) < 4){
	  home = 1;
	}
	else{
	  home = 0;
	}
	
	//printf("home = %d\n",home);
	
	/*
	if(OBS == NULL){
	  printf("Obstacle not created\n");
	  gui_state->DMM_STATE = 0;
	    break;
	    }*/
	
	if(state == 0){
	  
	  //printf("Going to rendevous/start position\n");  
	  pthread_mutex_lock(&servo_mutex);
	  user_cmds.speed      = 0.5;
	  user_cmds.torque     = 0.5;
	  user_cmds.servo[0]   = 0.0;
	  user_cmds.servo[1]   = 90.0;
	  user_cmds.servo[2]   = 0.0;
	  user_cmds.servo[3]   = 0.0;
	  user_cmds.play_speed = 1.0;
	  pthread_mutex_unlock(&servo_mutex);
	  
	  state = 1;
	}
	
	else if(state == 1){
	  
	  if(home){
	    //if at home go to state 2
	    
	    if(OBS!=NULL){
	      
	      obstacle_t *ptr1 = OBS;
	      obstacle_t *ptr2;
	      
	      while(ptr1->next != NULL){
		ptr2 = ptr1->next;
		free(ptr1);
		ptr1 = ptr2;
	      }
	      
	      free(ptr1);
	      
	      printf("Cleared obstacle list\n");
	    }
	    
	    CreateObsList(gui_state);
	    
	    obstacle_t *ptr = OBS;
	    
	    while(ptr){
	      printf("obstacle created\n");
	      ptr = ptr->next;
	    }
	    
	    
	    state = 2;
	  }
	  
	}
	else if(state == 2){
	  //Start planning
	  
	  printf("Creating roadmaps ...\n");

	  int add_points;
	  printf("started graph_create\n");
	  road_map = graph_create(); 
	  printf("finished graph_create\n");
	  printf("started graph_grid_points\n");
	  add_points        = graph_grid_points(road_map,OBS);
	  printf("finished graph_grid_points\n");
	  printf("started random_points\n");
	  random_points(road_map,OBS,add_points);
	  printf("finished random_points\n");
	  printf("started get_edges\n");
	  get_edges(road_map,OBS);
	  printf("finished get_edges\n");
	  
	  printf("Roadmap construction complete\n");
	  
	  double q0[4] = {0,90,0,0};
	  printf("started find_closest_point\n");
	  closest_qi = find_closest_point(road_map,q0,OBS);
	  printf("finished find_closest_point\n");
	  
	  printf("started Dijkstra\n");
	  Dijkstra(road_map,closest_qi);
	  printf("finished Dijkstra\n");
	  
	  
	  state = 3;
	  
	}
	else if(state == 3){
	  
	  //Wait for home
	  if(home){	    
	    printf("At home\n");
	    printf("Going to Target = %d\n",target);
	    state = 4;
	    start_rec = 0;
	  }
	}
	
	else if(state == 4){
	  
	  if(!start_rec){
	    gettimeofday(&rec_start_time, NULL);
	    start_rec = 1;
	  }
	  
	  gettimeofday(&cur_time, NULL);
	  
	  time = (cur_time.tv_sec - rec_start_time.tv_sec) +
	      1.0E-6*((double) (cur_time.tv_usec - rec_start_time.tv_usec));
	  
	  if(time > PAUSE_TIME){
	      //Pause for 5 sec   
	    
	    printf("PAUSE OVER\n");
	    state = 5;
	  }
	  
	}
	
	else if(state == 5){
	  
	  
	  //Execute
	  //int count = 1;
	  
	  if(target >= gui_state -> num_ann){
	    state = 7;
	    printf("Finished all targets\n");
	  }
	  
	  
	  if(target < gui_state ->num_ann){

	    targetPos[0] = gui_state->Apos[target].x;
	    targetPos[1] = gui_state->Apos[target].y;
	    targetPos[2] = GOAL_ALT;
	    
	    printf("staring planner for PRM\n");
	    
	    //ClearWayPoints();
	    
	    double qf[4]={90,30,30,30};
	    
	    printf("Querying road maps....\n");
	    
	    //while (ERROR_PF > 2 && count < 5){
	    ClearWayPoints();
	    
	    unsigned char graph_status;

	    graph_status = PRM_Planner(q_rend,qf,targetPos,OBS);
	    
	    if(!graph_status){
	      state = 3;
	      continue;
	    }
	    
	    //Get Next obstacle
	    target++;
	    
	    if(ERROR_PF > 3){
	      state = 3;
	      continue;
	    }

	    printf("Interpolating cubic splines\n");
	    Plan_Trajectory(0);
	    Plan_Trajectory(-2);
	      
	    
	    pthread_mutex_lock(&play_mutex);
	    PLAY = 1;
	    play = PLAY;
	    printf("play status = %d\n",play);
	    pthread_mutex_unlock(&play_mutex);
	    
	    while(play){
	      
	      pthread_mutex_lock(&play_mutex);
	      play = PLAY;
	      pthread_mutex_unlock(&play_mutex);
	      
	    }
	    
	    printf("Finished play\n");
	    state = 3;
	    
	    
	  }
	  
	  
	  
	  
	}
	
	else if(state == 6){
	  
	  //Stop state
	}
	
	
      }
      else{
	
	if(state == 6)
	  break;
	
      }
      
      
      
      
      
      
      
      
  }
  
  
  //Get state machine status from gui
  
  
  //Get arm position
  
  
  //Determine state

  
  return NULL;

}
