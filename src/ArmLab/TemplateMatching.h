#ifndef TMP_MATCH
#define TMP_MATCH


#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <string.h>


#include "../imagesource/image_u32.h"
#include "../imagesource/image_source.h"
#include "../imagesource/image_convert.h"

#define MAX_INT 65535


typedef struct obsList obsList_t;
struct obsList{
	int n_obs;
	int *cord;
};


int getX_obsList(obsList_t *oList, int n);

int getY_obsList(obsList_t *oList, int n);

void addCord_obsList(obsList_t *oList, int x, int y);

void getMatchLocations(image_u32_t * img, image_u32_t * temp_img, int * numMatches, obsList_t *temp_obs,double tolerance);

void minimaSupression(int init_x,int init_y,int  window_size_x,int window_size_y, image_u32_t * img,  unsigned long *arr, unsigned long *sup_arr,int tolerance);

void templateMatch(image_u32_t * img, image_u32_t * temp_img, unsigned long *err_arr);

void splitIntoComponents(uint32_t buff, uint8_t *comp);

int compute_err(uint32_t buff_q, uint32_t buff_t);

int compute_err(uint32_t buff_q, uint32_t buff_t);





#endif
