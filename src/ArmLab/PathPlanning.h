#ifndef PATH_PP_H
#define PATH_PP_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <math.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>

// common
#include "../common/getopt.h"
#include "../common/pg.h"
#include "../common/zarray.h"
#include "../math/gsl_util_matrix.h"
#include "../math/gsl_util_math.h"

#include "rexarm.h"

typedef struct obstacle obstacle_t;
struct obstacle{

    unsigned char obs_type;    //Obstacle type; 1.Cylinder, 2.Annulus, 3. Ceiling
    double x;                  //X position in world frame
    double y;                  //Y position in world frame
    double height;             //Height of obstalce
    double radius_O;           //Outer radius
    double radius_I;           //Inner radius ( =0 for Cylinder)

    obstacle_t *next;
};

typedef struct WayPoints WayPoints_t;
struct WayPoints{
    double time;
    double servo[4];
  WayPoints_t *prev;
    WayPoints_t *next;
};


typedef struct Trajectory Trajectory_t;
struct Trajectory{
    
    double t0;
    double tf;

    double servo1_a[4];
    double servo2_a[4];
    double servo3_a[4];
    double servo4_a[4];
    Trajectory_t *next;
};

/**
 *@brief gradient of objective function  
 *@param [in] *q  current configuration
 *@param [in] *xf final point
 *@param [in] *obs pointer to obstacle list
 *@return torque (gradient of the potential function)
 */
double pf_objfun(double *q,double *qf,double *xf,obstacle_t *obs, double *torque,unsigned char ex_pf);


/**
 *@brief Compute attractive potential/torque for control point
 *@param [in] *Oi   world co-ordinate of control point
 *@param [in] *Og   world co-ordinate of goal control point
 *@param [in] *J    jacobian matrix for control point
 *@param [in] dlen  distance of influence
 *@param [in] zeta  scaling parameter
 *@param [out] *u_att pointer to attractive potential
 *@param [out] *t_att pointer to attractive torque (3d)
 */
double pf_att(double *Oi,double *Og,gsl_matrix *J,
	    double dlen,double zeta,
	    double *t_att);

double pf_rep(double *O,gsl_matrix *J,double roh_0,double eta,obstacle_t Obs,int num,double *t_rep);



//Get Shortest distance from cylinder
unsigned char cc_check_cylinder(obstacle_t obs,double *point,int num);


unsigned char cc_check_annulus(obstacle_t obs,double *point,int num);

unsigned char cc_check_floor(obstacle_t obs,double *point);



/**
 * @brief Returns the L2 norm of a vector
 * @param [in] x n-dimensional vector (double array of size n)
 * @param [in] n dimension of vector
 * @return norm of vector x
 */
double L2norm(double *x,int n);


gsl_matrix * Compute_Jacobian(double *q,double *o1,double *o2,double *o3,
			      double *o4,double *oc,unsigned char num);

//Gradient descent
void gradientDescent(double q_start[4],double qf[4], double wgoal[3], obstacle_t * obs,unsigned char start,unsigned char ex_pf,double time);


//Get shortest distance from point O to plane (ceiling/table)
void roh_plane(double *O, obstacle_t Obs,double *roh);

void roh_cylinder(double *O, obstacle_t obs,int num,double *roh);

void roh_annulus(double *O, obstacle_t obs,int num,double *roh);


// isColliding_cylinder
bool isColliding(obstacle_t * obs);

void cross_product(const gsl_vector *u, const gsl_vector *v, gsl_vector *product);

void vector_diff(const gsl_vector *u,const gsl_vector *v,gsl_vector *w);

gsl_matrix * GetMatrix(const gsl_vector *J1,const gsl_vector *J2,
		       const gsl_vector *J3,const gsl_vector *J4,int i);


bool isPointColliding(double point_cord[3], obstacle_t * obs, double padding);


//double * getFloatingPoint(double x1, double y1, double x2, double y2, double xp, double yp);

bool isInCircle(double px, double py, double center_x, double center_y, double radius, double padding);


double getDistance(double x1, double y1, double x2, double y2);

double distSquared(double x1, double y1, double x2, double y2);

double getError(double qi[4],double qf[4], double wgoal[3],unsigned char ex_pf);

double fRand(double fMin, double fMax);

unsigned char cc_check(double *q, obstacle_t *obs);

void GetFloatingPoint(double *OA,double *OB,obstacle_t obs,double *FP);

double getLineCircleIntersection(double *P1,double *P2,double xc,double yc,double r,double *I1,double *I2);

void GetZ(double *OA,double *OB,double *I1);

void GetProj(double *OA,double *OB,double *OC,double *FP);

void IsInSegment(double *OA,double *OB,double *I1);

//double SelectStepSize(double *qi, double *tip_cord_final,obstacle_t *obs);

void createList_waypoints(double time, double *servo);

void addList_waypoints(double time, double *servo);

WayPoints_t * getLastNode_waypoints();

void delLastNode_waypoints();

void Plan_Trajectory(int reverse);

void populateAngles(double time, Trajectory_t* segment, double * q_t);

void populateTrajectoryCoeff(double q0, double qf, double v0,double vf, double t0, double tf,double* a);

Trajectory_t *getSegmentTrajectory(WayPoints_t *startPoints, WayPoints_t *endPoints,double TF,double TF2);


void ClearWayPoints();


#endif
