#ifndef GUI_H
#define GUI_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <math.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>


// core api
#include "../vx/vx.h"
#include "../vx/vx_util.h"
#include "../vx/vx_remote_display_source.h"
#include "../vx/gtk/vx_gtk_display_source.h"

// drawables
#include "../vx/vxo_drawables.h"

// common
#include "../common/getopt.h"
#include "../common/pg.h"
#include "../common/zarray.h"
#include "../math/gsl_util_matrix.h"

// imagesource
#include "../imagesource/image_u32.h"
#include "../imagesource/image_source.h"
#include "../imagesource/image_convert.h"
#include "../imagesource/image_util.h"

#include "../apps/rob550_util.h"    // This is where a lot of the internals live

#include "affine_calibration.h"

#define CYL_R 4.5
#define ANN_RO 3.75
#define ANN_RI 1.5
#define CYL_H 15
#define ANN_H 1.9

pthread_mutex_t servo_mutex;
pthread_mutex_t fdb_mutex;
pthread_mutex_t play_mutex;
pthread_mutex_t inverse_mutex;

typedef struct GUIcommands gui_cmds_t;
struct GUIcommands{

    double speed;       //Speed command 
    double torque;      //Torque command
    double servo[4];    //Servo angles - user supplied
    double cmd_x;       //End effector x position in workspace
    double cmd_y;       //End effector y position in workspace  
    double play_speed;
}; 

typedef struct CylPos cyl_pos_t;
struct CylPos{
    double x;
    double y;
};

typedef struct AnnPos ann_pos_t;
struct AnnPos{
    double x;
    double y;
};

// It's good form for every application to keep its state in a struct.
typedef struct GUIstate gui_state_t;
struct GUIstate {
    bool running;
    bool click_clr;
    
    getopt_t        *gopt;
    parameter_gui_t *pg;
    
    // image stuff
    char *img_url;
    int   img_height;
    int   img_width;
    
    // vx stuff
    vx_application_t    vxapp;
    vx_world_t         *vxworld;           // where vx objects are live
    vx_event_handler_t *vxeh;              // for getting mouse, key, and touch events
    vx_mouse_event_t    last_mouse_event;
    
    //Workspace calibration variables
    affine_pair_t calib_pts[5];
    unsigned char calib_state;
    unsigned char calib_complete;
    
    int num_cyl;
    cyl_pos_t Cpos[20];
    unsigned char draw_cyl;

    double temp_ann_start[4];
    double temp_ann_stop[4];

    double temp_cyl_start[4];
    double temp_cyl_stop[4];

    image_u32_t *template_annulus;
    image_u32_t *template_annulus_decimate;
    image_u32_t *template_cylinder;
    image_u32_t *template_cylinder_decimate;

    
    image_u32_t *snapshot;
    image_u32_t *snapshot_decimate;

    unsigned char DMM_STATE;

    int num_ann;
    ann_pos_t Apos[20];
    bool draw_ann;
    
    double Z;
    double PITCH;
    
    unsigned char ELBOW_UP;
    unsigned char REVERSE_CONFIG;
    unsigned char TEACH;
    unsigned char PLANNER_PF;
    unsigned char PLANNER_PRM;
    unsigned char GHOST;
    
    struct timeval start_time;
    
    // threads
    pthread_t animate_thread;
    
    // for accessing the arrays
    pthread_mutex_t mutex;
};



void my_param_changed (parameter_listener_t *pl, parameter_gui_t *pg, const char *name);

int mouse_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_mouse_event_t *mouse);

int key_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_key_event_t *key);

int touch_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_touch_event_t *mouse);

void * animate_thread (void *data);

gui_state_t * state_create (void);

void state_destroy (gui_state_t *state);

void CreateObsList(gui_state_t *state);






#endif
