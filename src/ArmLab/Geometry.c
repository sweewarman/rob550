#include "PathPlanning.h"

//#define DEBUG

extern double padding_cyl;    // padding for cylinder radius
extern double h_padd;    // padding for heights

extern double padding_O;
extern double padding_I;
extern double padding_O4;  //padding for link 4

void GetFloatingPoint(double *AA,double *BB,obstacle_t obs,double *FP){

    double temp[3];
    double I1[3],I2[3];
    double delta;

    double OA[3];
    double OB[3];

    memcpy(OA,AA,3*sizeof(double));
    memcpy(OB,BB,3*sizeof(double));

    double radius;
    unsigned char inside_annulus = 0;

    double D[3];

    double height = obs.height + h_padd;

#ifdef DEBUG
    printf("OA = %f %f %f\n",OA[0],OA[1],OA[2]);
    printf("OB = %f %f %f\n",OB[0],OB[1],OB[2]);
    
#endif

    // If point A is higher that B, swap them
    if(OA[2] >= OB[2]){

#ifdef DEBUG
	printf("OA is higher\n");
#endif

	memcpy(temp,OA,3*sizeof(double));
	
	OA[0] = OB[0];
	OA[1] = OB[1];
	OA[2] = OB[2];

	OB[0] = temp[0];
	OB[1] = temp[1];
	OB[2] = temp[2];
    }
    
    // Check if obstacle is a plane. Return nearest joint
    if(obs.obs_type == 3){

	double mag1 = fabs(OA[2] - obs.height);
	double mag2 = fabs(OB[2] - obs.height);

	if(mag1 <= mag2){
	    FP[0] = OA[0];
	    FP[1] = OA[1];
	    FP[2] = OA[2];
	}
	else{
	    FP[0] = OA[0];
	    FP[1] = OA[1];
	    FP[2] = OA[2];
	}

	return;
	    
    }

    radius = obs.radius_O;

    if(obs.obs_type == 2){
	if( isInCircle(OA[0],OA[1],obs.x,obs.y,obs.radius_I,0) ){
	    radius = obs.radius_I;
	    inside_annulus = 1;

#ifdef DEBUG
	    printf("setting Innder radius\n");
#endif
	    if( isInCircle(OB[0],OB[1],obs.x,obs.y,obs.radius_I,0) ){
		FP[0] = OA[0];
		FP[1] = OA[1];
		FP[2] = OA[2];

#ifdef DEBUG
		printf("setting Innder radius twice\n");
#endif
		return;

	    }
	}
	else{
	    inside_annulus = 0;
	}
    }
    
    delta = getLineCircleIntersection(OA,OB,obs.x,obs.y,radius,I1,I2);

#ifdef DEBUG
    printf("delta = %f\n",delta);
#endif
    if(delta>0.5){

#ifdef DEBUG	
	printf("checking for two intersections\n");
#endif
	GetZ(OA,OB,I1);
	GetZ(OA,OB,I2);
	
	IsInSegment(OA,OB,I1);
	IsInSegment(OA,OB,I2);
	
	if(!inside_annulus){
	    if(I1[2]<=I2[2]){
		FP[0] = I1[0];
		FP[1] = I1[1];
		FP[2] = I1[2];
	    }
	    else{
		FP[0] = I2[0];
		FP[1] = I2[1];
		FP[2] = I2[2];
	    }
	}
	else{
	     if(I1[2]<=I2[2]){
		FP[0] = I2[0];
		FP[1] = I2[1];
		FP[2] = I2[2];
	    }
	    else{
		FP[0] = I1[0];
		FP[1] = I1[1];
		FP[2] = I1[2];
	    }
	}

#ifdef DEBUG
	printf("intersection point elevated = %f %f %f\n",FP[0],FP[1],FP[2]);
#endif

	D[0] = FP[0];
	D[1] = FP[1];
	D[2] = height;
	
	GetProj(OA,OB,D,FP);

#ifdef DEBUG
	printf("floating point = %f %f %f\n",FP[0],FP[1],FP[2]);
#endif

	if(FP[2] < OA[2]){
	    FP[0] = OA[0];
	    FP[1] = OA[1];
	    FP[2] = OA[2];
	
	}
	if(FP[2] > OB[2]){
	    FP[0] = OB[0];
	    FP[1] = OB[1];
	    FP[2] = OB[2];

	}

	return;
	
	
	
    }
    else{

#ifdef DEBUG
	printf("Checking for outside intersection\n");
#endif	
	if( fabs(OB[0] - OA[0]) < 0.005){
	    IsInSegment(OA,OB,FP);

#ifdef DEBUG
	        printf("floating point = %f %f %f\n",FP[0],FP[1],FP[2]);
#endif
	    return;

	}

	double m1 = (OB[1] - OA[1])/(OB[0] - OA[0]);

	double m2 = -1/m1;

	double c1 = OA[1] - m1*OA[0];
	double c2 = obs.y    - m2*obs.x;

	FP[0] = (c2 - c1)/(m1 - m2);

	FP[1] = m1*FP[0] + c1;

	GetZ(OA,OB,FP);

	IsInSegment(OA,OB,FP);

#ifdef DEBUG
	printf("floating point = %f %f %f\n",FP[0],FP[1],FP[2]);
#endif

	return;
	}
    
}

double getLineCircleIntersection(double *P1,double *P2,double xc,double yc,double r,double *I1,double *I2){

    double x1 = P1[0] - xc;
    double y1 = P1[1] - yc;

    double x2 = P2[0] - xc;
    double y2 = P2[1] - yc;

    double dx = x2 - x1;
    double dy = y2 - y1;
    double dr = sqrt(dx*dx + dy*dy);

    double D  = x1*y2 - x2*y1;
    
    double delta = r*r * dr*dr - D*D;

    double sgn_dy = 1;

    double p1,p2,p3,p4;

#ifdef DEBUG
      printf("points A = %f %f %f, B = %f %f %f\n",P1[0],P1[1],P1[2],P2[0],P2[1],P2[2]);
#endif

    if(dr < 0.005){
	delta = 0;
	return delta;
    }
    
    if (dy <0 )
	sgn_dy = -1;

    p1 = (D*dy + sgn_dy*dx*sqrt(r*r*dr*dr - D))/(dr*dr) + xc;
    p3 = (D*dy - sgn_dy*dx*sqrt(r*r*dr*dr - D))/(dr*dr) + xc;

    p2 = (-D*dx + fabs(dy)*sqrt(r*r*dr*dr - D))/(dr*dr) + yc;
    p4 = (-D*dx - fabs(dy)*sqrt(r*r*dr*dr - D))/(dr*dr) + yc;

    if(delta == 0){
	I1[0] = p1;
	I1[1] = p2;
    }
    
    if(delta > 0){
	I1[0] = p1;
	I1[1] = p2;
	I2[0] = p3;
	I2[1] = p4;
    }

#ifdef DEBUG
    printf("delta = %f: Intersections are I1 = %f %f, I2 = %f %f\n ",delta,I1[0],I1[1],I2[0],I2[0]);
#endif
	
    return delta;

}


void GetZ(double *OA,double *OB,double *I1){
    

    double AB[3];
    double ab[3];
    double magAB;

    //distance to move along ab to get to point I1
    double t1;;      

    AB[0] = OB[0] - OA[0];
    AB[1] = OB[1] - OA[1];
    AB[2] = OB[2] - OA[2];

    magAB = L2norm(AB,3);

    ab[0] = AB[0]/magAB;
    ab[1] = AB[1]/magAB;
    ab[2] = AB[2]/magAB;

    t1    = (I1[0] - OA[0])/ab[0];

    I1[2] = OA[2] + t1*ab[2];

#ifdef DEBUG
    printf("z co-ordinate is %f\n",I1[2]);
#endif
}

void GetProj(double *OA,double *OB,double *OC,double *FP){

    double AC[3];
    double AB[3];
    double ab[3];
    double magAB;
    double proj;

    AC[0] = OC[0] - OA[0];
    AC[1] = OC[1] - OA[1];
    AC[2] = OC[2] - OA[2];

    AB[0] = OB[0] - OA[0];
    AB[1] = OB[1] - OA[1];
    AB[2] = OB[2] - OA[2];

    magAB = L2norm(AB,3);

    proj  = (AC[0]*AB[0] + AC[1]*AB[1] + AC[2]*AB[2])/magAB;

#ifdef DEBUG
    printf("Projection = %f\n",proj);
#endif
   

    ab[0] = AB[0]/magAB;
    ab[1] = AB[1]/magAB;
    ab[2] = AB[2]/magAB;

    FP[0] = OA[0] + proj*ab[0];
    FP[1] = OA[1] + proj*ab[1];
    FP[2] = OA[2] + proj*ab[2];


}


void IsInSegment(double *OA,double *OB,double *I1){

    if(I1[2] <= OA[2]){
	I1[0] = OA[0];
	I1[1] = OA[1];
	I1[2] = OA[2];

#ifdef DEBUG
	printf("given point is below A\n");
#endif
    }
    
    if(I1[2] > OB[2]){
	I1[0] = OB[0];
	I1[1] = OB[1];
	I1[2] = OB[2];

#ifdef DEBUG
	printf("given point is above B\n");
#endif
    }

    return;
}
