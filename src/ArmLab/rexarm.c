#ifndef REX_C
#define REX_C

// core api
#include "../vx/vx.h"
#include "../vx/vx_util.h"
#include "../vx/vx_remote_display_source.h"
#include "../vx/gtk/vx_gtk_display_source.h"
#include "../vx/vxo_mat.h"

// drawables
#include "../vx/vxo_drawables.h"

#include "gui.h"
#include "rexarm.h"
#include "PathPlanning.h"

extern gui_cmds_t user_cmds;
extern gui_cmds_t servo_fdb;

extern pthread_mutex_t servo_mutex;
extern pthread_mutex_t fdb_mutex;
extern pthread_mutex_t play_mutex;
extern pthread_mutex_t inverse_mutex;

extern double wp_scaling;
extern unsigned char PLAY;
extern WayPoints_t *waypoints;
extern Trajectory_t *trajectory;
extern int num_waypoints;

extern unsigned char INVERSE;
extern double inverse_q[4];

extern double origin_p[2];

// joint limits for clamp function
const double max_servo[4] = {180,120,120,120};
const double min_servo[4] = {-180,-120,-120,-120};
double prev_servo[4] = {0,0,0,0};

// constant DH parameters
const double d[4]     = {11.7,0,0,0};      // (cm)
const double a[4]     = {0,10,10,11.2};  // (cm)
const double alpha[4] = {90,0,0,0};      // (deg)
const double p4       = 0;               // (cm), end-effector position measured from 4th joint

Trajectory_t *CurrentTraj;

// Clamping function
void rexarm_clamp(double *qi,double *qo){

    int i=0;

 
    for (i=0;i<NUM_SERVOS;i++){
	if (qo[i]  > max_servo[i])
	    qo[i] = max_servo[i];
	else if (qo[i]   <min_servo[i])
	    qo[i] = min_servo[i];
    }
    

    double * tip_cord = tip_pos(qo[0], qo[1], qo[2], qo[3], 4);
    
    bool isIn = isPointInWorkspace(*tip_cord,*(tip_cord+1),*(tip_cord+2));
    
    if( !isIn  ){
	for (i=0;i<NUM_SERVOS;i++)
	    qo[i] = prev_servo[i];
    }
    else{
      for (i=0;i<NUM_SERVOS;i++)
	prev_servo[i] = qo[i];
    }
	free(tip_cord);
}


// Check if point is in workspace
bool isPointInWorkspace(double x,double y,double z)
{
  
  //printf ("x:%lf    y:%lf    z:%lf\n",x,y,z);
    
    bool isIn =true;
    
    //printf("x,y = %f,%f\n",x,y);
    
    if( z < 0.1){
	isIn = false;    
	//printf("Not in workspace\n");
    }
    else if(z >= 11.0){
	isIn = true;
	// printf("In workspace 1\n");
	
    }
    else  
	if( ( (x > -2.5) && (x < 2.5) ) && ( (y > -1.5) && (y < 4.5) )){
	    isIn = false;
	    // printf("In workspace 2\n");
	    
	}
        
    return isIn;
    
}

void status_handler (const lcm_recv_buf_t *rbuf,
                const char *channel,
                const dynamixel_status_list_t *msg,
                void *user)
{


    // Print out servo positions
    for (int id = 0; id < msg->len; id++) {
        dynamixel_status_t stat = msg->statuses[id];
        //printf ("[id %d]=%6.3f ",id, stat.position_radians*180/M_PI);

	pthread_mutex_lock(&fdb_mutex);
	servo_fdb.servo[id] = stat.position_radians * 180/M_PI;
	pthread_mutex_unlock(&fdb_mutex);

    }

    
    //printf ("\n");
}

void * status_loop (void *data)
{
    rex_state_t *state = data;
    dynamixel_status_list_t_subscribe (state->lcm,
                                       state->status_channel,
                                       status_handler,
                                       state);
    const int hz = 15;
    while (state->running) {
        // Set up the LCM file descriptor for waiting. This lets us monitor it
        // until something is "ready" to happen. In this case, we are ready to
        // receive a message.
        int status = lcm_handle_timeout (state->lcm, 1000/hz);
        if (status <= 0)
            continue;
	
        // LCM has events ready to be processed
    }

    return NULL;
}

void * command_loop (void *user)
{
    rex_state_t *state = user;
    const int hz = 30;
    dynamixel_command_list_t cmds;
    cmds.len      = NUM_SERVOS;
    cmds.commands = calloc (NUM_SERVOS, sizeof(dynamixel_command_t));
    
    pthread_mutex_lock(&servo_mutex);
    memset(user_cmds.servo,0,4*sizeof(double));
    user_cmds.speed      = 0;
    user_cmds.torque     = 0;
    pthread_mutex_unlock(&servo_mutex);
    
    while (state->running) {
        // Send LCM commands to arm. Normally, you would update positions, etc,
        // but here, we will just home the arm.

      unsigned char play;

      pthread_mutex_lock(&play_mutex);
      play = PLAY; 
      pthread_mutex_unlock(&play_mutex);
	
      if(play){
	  //printf("Playing trajectory\n");
	  
	  play = PlayTrajectory();
	  
      }
      
	pthread_mutex_lock(&inverse_mutex);
	if(INVERSE){
	    user_cmds.servo[0] = inverse_q[0];
	    user_cmds.servo[1] = inverse_q[1];
	    user_cmds.servo[2] = inverse_q[2];
	    user_cmds.servo[3] = inverse_q[3];
	    pthread_mutex_unlock(&inverse_mutex);
	    // printf("Inverse q angles = %f, %f, %f, %f\n",inverse_q[0],inverse_q[1],inverse_q[2],inverse_q[3]);
	}
	else{
	    pthread_mutex_unlock(&inverse_mutex);
	}

	
	pthread_mutex_lock(&servo_mutex);
	rexarm_clamp(servo_fdb.servo,user_cmds.servo);
	pthread_mutex_unlock(&servo_mutex);

	

        for (int id = 0; id < NUM_SERVOS; id++) {
	    // home servos slowly
	    cmds.commands[id].utime            = utime_now ();
	    
	    pthread_mutex_lock(&servo_mutex);
	    cmds.commands[id].position_radians = user_cmds.servo[id] * PI/180;
	    cmds.commands[id].speed            = user_cmds.speed;
	    cmds.commands[id].max_torque       = user_cmds.torque;
	    pthread_mutex_unlock(&servo_mutex);
            
        }
        dynamixel_command_list_t_publish (state->lcm, state->command_channel, &cmds);

        usleep (1000000/hz);

    }

    free (cmds.commands);

    return NULL;
}


vx_object_t * rexarm_vxo_arm(const int errors[4],const double angles[4],const float arm_color[4],float alpha){
    
    vx_object_t *vxo_arm0;
    vx_object_t *vxo_arm1;
    vx_object_t *vxo_arm2;
    vx_object_t *vxo_arm3;
    vx_object_t *vxo_arm4;
    
    double dia1 = 4;
    double len1 = 5;
    double ax   = 10;
    double wx   = 3;
    double wy   = 3;
    
    double* T01  = mat_for_vx(angles[0],angles[1],angles[2],angles[3],1);
    double* T02  = mat_for_vx(angles[0],angles[1],angles[2],angles[3],2);
    double* T03  = mat_for_vx(angles[0],angles[1],angles[2],angles[3],3);
    double* T04  = mat_for_vx(angles[0],angles[1],angles[2],angles[3],4);
    
    
    vxo_arm1 = vxo_chain(vxo_mat_translate3(origin_p[0],origin_p[1],0),vxo_mat_scale(wp_scaling),
			   vxo_mat_copy_from_doubles( T01 ),
			   vxo_chain(vxo_mat_scale3(dia1,dia1,len1),vxo_cylinder(vxo_mesh_style (vx_blue))),
			   vxo_chain(vxo_mat_scale3(wy,ax+1,wx),vxo_mat_translate3(-0.0,-0.5,0.0),vxo_box(vxo_mesh_style (vx_blue))));

    vxo_arm2 = vxo_chain(vxo_mat_translate3(origin_p[0],origin_p[1],0),vxo_mat_scale(wp_scaling),
			   vxo_mat_copy_from_doubles( T02 ),
			   vxo_chain(vxo_mat_scale3(dia1,dia1,len1),vxo_cylinder(vxo_mesh_style (vx_blue))),
			   vxo_chain(vxo_mat_scale3(ax,wx,wy),vxo_mat_translate3(-0.5,-0.0,0.0),vxo_box(vxo_mesh_style (vx_blue))));

    vxo_arm3 = vxo_chain(vxo_mat_translate3(origin_p[0],origin_p[1],0),vxo_mat_scale(wp_scaling),
			   vxo_mat_copy_from_doubles( T03 ),
			   vxo_chain(vxo_mat_scale3(dia1,dia1,len1),vxo_cylinder(vxo_mesh_style (vx_blue))),
			   vxo_chain(vxo_mat_scale3(ax,wx,wy),vxo_mat_translate3(-0.5,-0.0,0.0),vxo_box(vxo_mesh_style (vx_blue))));

    vxo_arm4 = vxo_chain(vxo_mat_translate3(origin_p[0],origin_p[1],0),vxo_mat_scale(wp_scaling),
			   vxo_mat_copy_from_doubles( T04 ),
			   vxo_chain(vxo_mat_scale3(ax/4,wx,wy),vxo_mat_translate3(-3-0.5,-0.0,0.0),vxo_box(vxo_mesh_style (vx_blue))),
			   vxo_chain(vxo_mat_scale3(ax*3/4,0.5,0.5),vxo_mat_translate3(-0.5,-0.0,0.0),vxo_box(vxo_mesh_style (vx_green))));


    vxo_arm0 = vxo_chain_create();

    vxo_chain_add(vxo_arm0,vxo_arm1,vxo_arm2,vxo_arm3,vxo_arm4);
    

    free(T01);
    free(T02);
    free(T03);
    free(T04);
    
           
    return vxo_arm0;


}

gsl_matrix * H(double theta, double d, double a, double alpha)
{
    
    alpha = alpha*M_PI/180;
    theta = theta*M_PI/180;
    
    gsl_matrix * A = gsl_matrix_alloc (4,4);
    
    gsl_matrix_set(A,0,0,cos(theta)); 
    gsl_matrix_set(A,0,1,-sin(theta)*cos(alpha)); 
    gsl_matrix_set(A,0,2,sin(theta)*sin(alpha)); 
    gsl_matrix_set(A,0,3,a*cos(theta));
    gsl_matrix_set(A,1,0,sin(theta)); 
    gsl_matrix_set(A,1,1,cos(theta)*cos(alpha)); 
    gsl_matrix_set(A,1,2,-cos(theta)*sin(alpha)); 
    gsl_matrix_set(A,1,3,a*sin(theta));
    gsl_matrix_set(A,2,0,0); 
    gsl_matrix_set(A,2,1,sin(alpha)); 
    gsl_matrix_set(A,2,2,cos(alpha)); 
    gsl_matrix_set(A,2,3,d);
    gsl_matrix_set(A,3,0,0); 
    gsl_matrix_set(A,3,1,0); 
    gsl_matrix_set(A,3,2,0); 
    gsl_matrix_set(A,3,3,1);
    
    return A;

}

double * mat_for_vx(double theta1, double theta2, double theta3, double theta4, int i)
{
  double *output;
  
  output = (double *) malloc(16*sizeof(double));
  theta1 -=90;
  theta2 +=90;
  
  gsl_matrix * A01 = H(theta1,d[0],a[0],alpha[0]);
  gsl_matrix * A12 = H(theta2,d[1],a[1],alpha[1]);
  gsl_matrix * A23 = H(theta3,d[2],a[2],alpha[2]);
  gsl_matrix * A34 = H(theta4,d[3],a[3],alpha[3]);
  gsl_matrix * A = gsl_matrix_alloc (4,4);
  
  if (i==1)
      gsl_matrix_memcpy(A,A01);
  else if (i==2)
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, A01, A12,0.0, A);
  else if (i==3){
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, A01, A12,0.0, A);
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, A, A23,0.0, A34);
      gsl_matrix_memcpy(A,A34);
  }
  else if (i==4){
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, A01, A12,0.0, A);
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, A, A23,0.0, A01);
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, A01, A34,0.0, A);
  }
  
  output[0]  = gsl_matrix_get(A,0,0); 
  output[1]  = gsl_matrix_get(A,0,1); 
  output[2]  = gsl_matrix_get(A,0,2); 
  output[3]  = gsl_matrix_get(A,0,3);
  output[4]  = gsl_matrix_get(A,1,0); 
  output[5]  = gsl_matrix_get(A,1,1); 
  output[6]  = gsl_matrix_get(A,1,2); 
  output[7]  = gsl_matrix_get(A,1,3);
  output[8]  = gsl_matrix_get(A,2,0); 
  output[9]  = gsl_matrix_get(A,2,1); 
  output[10] = gsl_matrix_get(A,2,2); 
  output[11] = gsl_matrix_get(A,2,3);
  output[12] = gsl_matrix_get(A,3,0); 
  output[13] = gsl_matrix_get(A,3,1); 
  output[14] = gsl_matrix_get(A,3,2); 
  output[15] = gsl_matrix_get(A,3,3);
  
  gsl_matrix_free(A01);
  gsl_matrix_free(A12);
  gsl_matrix_free(A23);
  gsl_matrix_free(A34);
  gsl_matrix_free(A);
  
  return output;
}



double * tip_pos(double theta1, double theta2, double theta3, double theta4, int i)
{

  double  *output;
  output  = (double *) malloc(3*sizeof(double));
  theta1 -=90;
  theta2 +=90;

  gsl_matrix * A01 = H(theta1,d[0],a[0],alpha[0]);
  gsl_matrix * A12 = H(theta2,d[1],a[1],alpha[1]);
  gsl_matrix * A23 = H(theta3,d[2],a[2],alpha[2]);
  gsl_matrix * A34 = H(theta4,d[3],a[3],alpha[3]);
  gsl_vector * p1 = gsl_vector_alloc(4);
  gsl_vector * p2 = gsl_vector_alloc(4);

  gsl_vector_set(p1,1,0);
  gsl_vector_set(p1,2,0);
  gsl_vector_set(p1,3,1);
  
  if(i==1){
      output[0] = gsl_matrix_get(A01,0,3); 
      output[1] = gsl_matrix_get(A01,1,3); 
      output[2] = gsl_matrix_get(A01,2,3);


      return output;
  }
  if (i==2){
      gsl_vector_set(p1,0,0);
      gsl_blas_dgemv (CblasNoTrans,1.0, A12, p1,0.0, p2); 
      gsl_blas_dgemv (CblasNoTrans,1.0, A01, p2,0.0, p1); 
  }
  else if(i==3){
      gsl_vector_set(p1,0,0);
      gsl_blas_dgemv (CblasNoTrans,1.0, A23, p1,0.0, p2);  
      gsl_blas_dgemv (CblasNoTrans,1.0, A12, p2,0.0, p1); 
      gsl_blas_dgemv (CblasNoTrans,1.0, A01, p1,0.0, p2);
      gsl_vector_memcpy(p1,p2);	
  }
  else if (i==4){
      gsl_vector_set(p1,0,p4);
      gsl_blas_dgemv (CblasNoTrans,1.0, A34, p1,0.0, p2);
      gsl_blas_dgemv (CblasNoTrans,1.0, A23, p2,0.0, p1);  
      gsl_blas_dgemv (CblasNoTrans,1.0, A12, p1,0.0, p2); 
      gsl_blas_dgemv (CblasNoTrans,1.0, A01, p2,0.0, p1); 
  }
  
  output[0] = gsl_vector_get(p1,0);
  output[1] = gsl_vector_get(p1,1);
  output[2] = gsl_vector_get(p1,2);
  
  gsl_matrix_free(A01);
  gsl_matrix_free(A12);
  gsl_matrix_free(A23);
  gsl_matrix_free(A34);
  gsl_vector_free(p1);
  gsl_vector_free(p2);
  
  return output;

}

unsigned char PlayTrajectory(){

    struct timeval cur_time;
    static struct timeval rec_start_time;
    
    static unsigned char start_rec = 0;

    if(!start_rec){
	CurrentTraj = trajectory;
    }

    double time = 0;
    double t    = 0;
    
    double q_t[4];

    if(!start_rec){
	gettimeofday(&rec_start_time, NULL);
	start_rec = 1;
    }

    gettimeofday(&cur_time, NULL);

    time = (cur_time.tv_sec - rec_start_time.tv_sec) +
	1.0E-6*((double) (cur_time.tv_usec - rec_start_time.tv_usec));
    
    time = time * (user_cmds.play_speed);


    while(CurrentTraj){
	if(time > CurrentTraj->tf){
	    CurrentTraj = CurrentTraj->next;
	}
	else
	    break;
    }

    
   
    if(!CurrentTraj){
	
	printf("Reached end of trajectory\n");

	start_rec = 0;
	
	pthread_mutex_lock(&play_mutex);
	PLAY      = 0;
	pthread_mutex_unlock(&play_mutex);
		
	return 0;
	
    }
      
    
    //printf("time = %f %f,\n",time,CurrentTraj->t0);

    t = (time - CurrentTraj->t0);

    populateAngles(t,CurrentTraj,q_t);

    pthread_mutex_lock(&servo_mutex);
    for(int i=0;i<4;i++){
      user_cmds.servo[i] = q_t[i]*180/M_PI;
    }
    //printf("time = %f,angles = %f,%f,%f,%f\n",t,q_t[0]*180/M_PI,q_t[1]*180/M_PI,q_t[2]*180/M_PI,q_t[3]*180/M_PI);
    pthread_mutex_unlock(&servo_mutex);

    return 1;

}


double * inv_kin(double xg, double yg, double zg, double phi, int pos, double *prev_servo)
{
 

  printf("xg,yg,zg,phi,pos = %lf,  %lf,  %lf,  %lf,  %d\n",xg,yg,zg,phi,pos);

 
  phi = phi*M_PI/180;
  
  double  *output;
  output  = (double *) malloc(4*sizeof(double));
  double theta1, theta2, theta3, theta4, rg, res;
  double L1 = d[0];
  double L2 = a[1];
  double L3 = a[2];
  double L4 = a[3];

  

  
  theta1 = atan2(-xg,yg);
  rg = sqrt(xg*xg+yg*yg);
  
  double zg_p = zg + L4*sin(phi);
  
  rg = rg - L4*cos(phi);
  
  double dr = rg;
  double dz = zg_p-L1;
  
  res = (dz*dz+dr*dr-L2*L2-L3*L3)/(2*L2*L3);
  
  if (res>1 || res<-1){
    printf("theta3: no solution for xg = %f,   yg = %f,   zg = %f;  res = %f\n",xg,yg,zg,res);
    output[0] = prev_servo[0]; output[1] = prev_servo[1]; output[2] = prev_servo[2]; output[3] = prev_servo[3];
    return output;
  }
  theta3 = acos(res);
  
  if (pos == 2 || pos==4) // ellbow down
    theta3 = -theta3;
  
  double beta = atan2(dz,dr);
  res = (L3*L3-(dz*dz+dr*dr)-L2*L2)/(-2*L2*sqrt(dz*dz+dr*dr));
  
  if (res>1 || res<-1){
    printf("theta2: no solution for xg = %f,   yg = %f,   zg = %f\n",xg,yg,zg);
    output[0] = prev_servo[0]; output[1] = prev_servo[1]; output[2] = prev_servo[2]; output[3] = prev_servo[3];
    return output;
  }
 

  double psi = acos(res);
  
  if (pos == 2 || pos==4) // ellbow down
    theta2 = M_PI/2 - beta + psi;
  else
    theta2 = M_PI/2 - beta - psi;
  theta4 = phi-theta2-theta3+ M_PI/2; 
  if (pos>2){
    
    if(theta1>0)
      theta1 -= M_PI;

    if(theta1<0)
      theta1 += M_PI;

    if(xg<0)
      theta1 -= M_PI;


    theta2 = -theta2;
    theta3 = -theta3;
    theta4 = -theta4;
  }

  

 
  output[0] = theta1*180/M_PI; 
  output[1] = theta2*180/M_PI; 
  output[2] = theta3*180/M_PI; 
  output[3] = theta4*180/M_PI;


  printf("inv angles in function %lf,%lf,%lf,%lf \n",output[0],output[1],output[2],output[3]);

  return output;
}




#endif
