#ifndef __AFFINE_CALIBRATION_H__
#define __AFFINE_CALIBRATION_H__


#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>


#include "common/zarray.h"
#include "math/matd.h"


#include "../math/gsl_util_matrix.h"


#ifdef __cplusplus
extern "C" {
#endif

#define CALIB_X_1 10
#define CALIB_Y_1 10
#define CALIB_X_2 10
#define CALIB_Y_2 10
#define CALIB_X_3 10
#define CALIB_Y_3 10
#define CALIB_X_4 10
#define CALIB_Y_4 10

typedef struct affine_pair affine_pair_t;
struct affine_pair {
    double pixel[2];
    double world[2];
};

gsl_matrix * get_conversion_mat(affine_pair_t *calib_pts);

gsl_matrix * get_inverse_of_conversion_mat(gsl_matrix *A);

void affine_calibration_w2p (const double world[2], double pixels[2]);

void affine_calibration_p2w (const double world[2], double pixels[2]);

  /*
typedef struct _affine_calibration_t affine_calibration_t;

// init/destroy
affine_calibration_t *
affine_calibration_create (const zarray_t *pairs);
void
affine_calibration_destroy (affine_calibration_t *cal);

// functions to go pixel->world and world->pixel
void
affine_calibration_p2w (const affine_calibration_t *cal, 
                        const double pixels[2], double world[2]);
void
affine_calibration_w2p (const affine_calibration_t *cal, 
                        const double world[2], double pixels[2]);

// useful for Vx
const matd_t *
affine_calibration_p2w_mat (const affine_calibration_t *cal);
const matd_t *
affine_calibration_w2p_mat (const affine_calibration_t *cal);
gsl_matrix_const_view
affine_calibration_p2w_gsl (const affine_calibration_t *cal);
gsl_matrix_const_view
affine_calibration_w2p_gsl (const affine_calibration_t *cal);


#ifdef __cplusplus
}
#endif

  */
#endif //__AFFINE_CALIBRATION_H__
