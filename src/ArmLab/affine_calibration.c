#include "affine_calibration.h"

gsl_matrix * conv_Mat;
gsl_matrix * inv_conv_Mat;

double wp_scaling; 

#define YUP      31
#define YDOWN   -29.8
#define XLEFT   -30.4
#define XRIGHT   30.4

//Get conversion matrix
gsl_matrix * get_conversion_mat(affine_pair_t *calib_pts)
{

  const double w_x[5] = {0, XLEFT, XRIGHT , XRIGHT,XLEFT};
  const double w_y[5] = {0, YUP,   YUP     ,YDOWN,YDOWN};
  int i,n=10,m=6; 

  gsl_vector * b = gsl_vector_alloc (n);
  gsl_matrix * A = gsl_matrix_alloc (n,m);

  for (i=0;i<n/2;i++)
    {
	// 2i-th row
	gsl_matrix_set(A,2*i,0,calib_pts[i].pixel[0]);
	gsl_matrix_set(A,2*i,1,calib_pts[i].pixel[1]);
	gsl_matrix_set(A,2*i,2,1);gsl_matrix_set(A,2*i,3,0);
	gsl_matrix_set(A,2*i,4,0);gsl_matrix_set(A,2*i,5,0);
	// (2i+1)-th row:
	gsl_matrix_set(A,2*i+1,0,0);gsl_matrix_set(A,2*i+1,1,0);gsl_matrix_set(A,2*i+1,2,0);
	gsl_matrix_set(A,2*i+1,3,calib_pts[i].pixel[0]);
	gsl_matrix_set(A,2*i+1,4,calib_pts[i].pixel[1]);
	gsl_matrix_set(A,2*i+1,5,1);
	gsl_vector_set(b,2*i,w_x[i]);
	gsl_vector_set(b,2*i+1,w_y[i]);
    }
  
  gsl_vector * bT = gsl_vector_alloc (m);
  gsl_vector * x  = gsl_vector_alloc (m);
  gsl_matrix * C  = gsl_matrix_alloc (m,m);
  
  gsl_blas_dgemm (CblasTrans, CblasNoTrans,1.0, A, A,0.0, C);
  gsl_blas_dgemv (CblasTrans,1.0, A, b,0.0, bT);  
  gsl_linalg_HH_solve(C,bT,x);
  
  gsl_matrix_free(C);
  gsl_matrix_free(A);
  gsl_vector_free(b);
  gsl_vector_free(bT);
  
  gsl_matrix * conv_Mat = gsl_matrix_alloc (3,3);
  
  gsl_matrix_set(conv_Mat,0,0,gsl_vector_get(x,0));
  gsl_matrix_set(conv_Mat,0,1,gsl_vector_get(x,1));
  gsl_matrix_set(conv_Mat,0,2,gsl_vector_get(x,2));
  gsl_matrix_set(conv_Mat,1,0,gsl_vector_get(x,3));
  gsl_matrix_set(conv_Mat,1,1,gsl_vector_get(x,4));
  gsl_matrix_set(conv_Mat,1,2,gsl_vector_get(x,5));
  gsl_matrix_set(conv_Mat,2,0,0);
  gsl_matrix_set(conv_Mat,2,1,0);
  gsl_matrix_set(conv_Mat,2,2,1);
  
  gsl_vector_free(x);
  
  return conv_Mat;
}


// Get inverse conversion matrix
gsl_matrix * get_inverse_of_conversion_mat(gsl_matrix *A)
{
    
    int s,n=3;
    gsl_matrix * inverse   = gsl_matrix_alloc (n, n);
    gsl_permutation * perm = gsl_permutation_alloc (n);
    
    gsl_linalg_LU_decomp (A, perm, &s);
    gsl_linalg_LU_invert (A, perm, inverse);
    gsl_permutation_free(perm);
    return inverse;
}


// Get transformation from world to pixel
void affine_calibration_w2p (const double world[2], double pixels[2])
{
  
    gsl_vector * w = gsl_vector_alloc (3);
    gsl_vector * p = gsl_vector_alloc (3);
  
    gsl_vector_set(w,0,world[0]);
    gsl_vector_set(w,1,world[1]);
    gsl_vector_set(w,2,1);
    gsl_blas_dgemv (CblasNoTrans,1.0, inv_conv_Mat, w,0.0, p);
    
    pixels[0] =  gsl_vector_get(p,0);  
    pixels[1] =  gsl_vector_get(p,1);
    gsl_vector_free(p);gsl_vector_free(w);

}

void affine_calibration_p2w (const double pixels[2], double world[2])
{
  
    gsl_vector * w = gsl_vector_alloc (3);
    gsl_vector * p = gsl_vector_alloc (3);
  
    gsl_vector_set(p,0,pixels[0]);
    gsl_vector_set(p,1,pixels[1]);
    gsl_vector_set(p,2,1);
    gsl_blas_dgemv (CblasNoTrans,1.0, conv_Mat, p,0.0, w);
    
    world[0] =  gsl_vector_get(w,0);  
    world[1] =  gsl_vector_get(w,1);
    
    gsl_vector_free(p);gsl_vector_free(w);
}
