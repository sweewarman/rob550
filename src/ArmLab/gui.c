#ifndef GUI_C
#define GUI_C

#include "gui.h"
#include "rexarm.h"
#include "PathPlanning.h"
#include "PRM.h"
#include "TemplateMatching.h"


#define CAMERA
//#define PF_DEBUG
#define PRM
//#define TEMP_DEBUG

gui_cmds_t user_cmds;
gui_cmds_t servo_fdb;

int num_waypoints = 0;
extern WayPoints_t *waypoints;
extern Trajectory_t *trajectory;

unsigned char PLAY;

extern gsl_matrix * conv_Mat;
extern gsl_matrix * inv_conv_Mat;
extern double wp_scaling;
extern double prev_servo[4];
extern obstacle_t *OBS;

double origin_p[2];

double inverse_q[4];
double inverse_input_p[2];
double inverse_input_w[2];

double planner_xg[3] = {0,0,0};

unsigned char render_inv;
unsigned char INVERSE = 0;

extern double FP_gui[3];
extern graph_t *road_map;

unsigned char template_annulus = 0;
unsigned char extract_annulus = 0;
unsigned char ann_temp_created = 0;

unsigned char template_cylinder = 0;
unsigned char extract_cylinder = 0;
unsigned char cyl_temp_created = 0;

int closest_qi;

double GOAL_ALT = 0.45;

unsigned char Graph_created = 0;

pthread_mutex_t DMM_mutex;

#define TOLERANCE_ANN 1200 //1700 
#define TOLERANCE_CYL 2800 

#define DECIMATE 3

// === Parameter listener =================================================
// This function is handed to the parameter gui (via a parameter listener)
// and handles events coming from the parameter gui. The parameter listener
// also holds a void* pointer to "impl", which can point to a struct holding
// state, etc if need be.
void my_param_changed (parameter_listener_t *pl, parameter_gui_t *pg, const char *name)
{
    
    gui_state_t *state = pl->impl;

    struct timeval cur_time;

    static struct timeval rec_start_time;

    static unsigned char start_rec = 0;
    
    double time;

    if (0==strcmp ("sl1", name)){
        //printf ("sl1 = %f\n", pg_gd (pg, name));
	pthread_mutex_lock(&servo_mutex);
	user_cmds.servo[0] = pg_gd (pg, name);
	pthread_mutex_unlock(&servo_mutex);
    }
    else if (0==strcmp ("sl2", name)){
        //printf ("sl2 = %d\n", pg_gi (pg, name));
	pthread_mutex_lock(&servo_mutex);
	user_cmds.servo[1] = pg_gd (pg, name);
	pthread_mutex_unlock(&servo_mutex);
    }
    else if (0==strcmp ("sl3", name)){
        //printf ("sl3 = %d\n", pg_gi (pg, name));
	pthread_mutex_lock(&servo_mutex);
	user_cmds.servo[2] = pg_gd (pg, name);
	pthread_mutex_unlock(&servo_mutex);
    }
    else if (0==strcmp ("sl4", name)){
        //printf ("sl4 = %d\n", pg_gi (pg, name));
	pthread_mutex_lock(&servo_mutex);
	user_cmds.servo[3] = pg_gd (pg, name);
	pthread_mutex_unlock(&servo_mutex);
    }
    else if (0==strcmp ("db1", name)){
        //printf ("%s = %f\n", name, pg_gd_boxes (pg, name));
	pthread_mutex_lock(&servo_mutex);
	user_cmds.speed  = pg_gd_boxes (pg, name);
	pthread_mutex_unlock(&servo_mutex);
    }
    else if (0==strcmp ("db2", name)){
        //printf ("%s = %f\n", name, pg_gd_boxes (pg, name));
	pthread_mutex_lock(&servo_mutex);
	user_cmds.torque  = pg_gd_boxes (pg, name);
	pthread_mutex_unlock(&servo_mutex);
    }
    else if (0==strcmp ("db3", name)){
        //printf ("%s = %f\n", name, pg_gd_boxes (pg, name));
	pthread_mutex_lock(&servo_mutex);
	user_cmds.play_speed  = pg_gd_boxes (pg, name);
	pthread_mutex_unlock(&servo_mutex);
    }
    else if (0==strcmp ("but1", name)){
        //printf ("%s clicked\n", name);
	state->calib_state = 1;
    }
    else if (0==strcmp ("but2", name)){
        //printf ("%s clicked\n", name);
	state->calib_state = 2;
    }
    else if (0==strcmp ("but3", name)){
        //printf ("%s clicked\n", name);
	state->calib_state = 3;
    }
    else if (0==strcmp ("but4", name)){
        //printf ("%s clicked\n", name);
	state->calib_state = 4;
    }
    else if (0==strcmp ("but5", name)){
        //printf ("%s clicked\n", name);
	//state->calib_state = 4;

      printf("calib state = %d\nCALIBRATING.......\n",state->calib_state);
	
      if(state->calib_state > 5){
	    // affine calibration (linear regression) goes in here	   
	    
	    double radius_w[2] = {1000000.0,0};
	    double radius_p[2];
	    
	    double origin_w[2] = {0,0};
	    	    
	    conv_Mat     = get_conversion_mat(state->calib_pts);
	    inv_conv_Mat = gsl_matrix_alloc(3,3);
	    
	    gslu_matrix_inv(inv_conv_Mat,conv_Mat);
	    state->calib_complete = 1;
	    
	    affine_calibration_w2p(radius_w,radius_p);

	    affine_calibration_w2p(origin_w,origin_p);
	    wp_scaling = sqrt(radius_p[0]*radius_p[0] + radius_p[1]*radius_p[1])/1000000;
	  
	    
	    state->calib_complete = 1;

	    printf("calibration complete\n");
	}
    }
    else if (0==strcmp ("but6", name)){
	//printf ("%s clicked\n", name);
	
	state->draw_cyl = 1;
    }
    else if (0==strcmp ("but7", name)){
	//printf ("%s clicked\n", name);
	
	state->draw_ann = 1;
    }
    else if (0==strcmp ("but8", name)){
	// Clear
	if(state->TEACH)
	    num_waypoints    = 0;
	else{
	    //printf ("%s clicked\n", name);
	    state->num_cyl  = 0;
	    state->num_ann  = 0;
	    state->draw_cyl = FALSE;
	    state->draw_ann = FALSE;
	    state->click_clr = true;
	}
	
    }
    else if(0==strcmp ("but9", name)){ 
	//Record way point
           
      double temp_servo[4];
      
      if(!start_rec){
	gettimeofday(&rec_start_time, NULL);
	start_rec = 1;
      }
      
      gettimeofday(&cur_time, NULL);
      
      time = (cur_time.tv_sec - rec_start_time.tv_sec) +
	1.0E-6*((double) (cur_time.tv_usec - rec_start_time.tv_usec));
      
      
      if(state->TEACH){
	
	
	for(int i=0;i<4;i++){
	  
	  pthread_mutex_lock(&fdb_mutex);
	  temp_servo[i] = servo_fdb.servo[i];
	  pthread_mutex_unlock(&fdb_mutex);
	  
	  printf("Waypoint %d, time = %f, angle %d = %f\n",num_waypoints,time,i,temp_servo[i]);
	  
	}
	
	if(num_waypoints==0){
	  createList_waypoints(time, temp_servo);
	}
	else{
	  addList_waypoints(time, temp_servo);
	}
			
	num_waypoints++;
      }
    }
    else if(0==strcmp ("but10", name)){ 
      //Call curve fitting function

      double qa[4] = {0,0,0,0};
      double xf[3] = {planner_xg[0],planner_xg[1],GOAL_ALT};
      
      if(!state->GHOST){
	pthread_mutex_lock(&fdb_mutex);
	qa[0] = servo_fdb.servo[0];
	qa[1] = servo_fdb.servo[1];
	qa[2] = servo_fdb.servo[2];
	qa[3] = servo_fdb.servo[3];
	pthread_mutex_unlock(&fdb_mutex);
      }
      else{
	pthread_mutex_lock(&servo_mutex);
	qa[0] = user_cmds.servo[0];
	qa[1] = user_cmds.servo[1];
	qa[2] = user_cmds.servo[2];
	qa[3] = user_cmds.servo[3];
	pthread_mutex_unlock(&servo_mutex);
      }
      
      if(state->TEACH){

	printf("staring planner for teach n repeat\n");

	Plan_Trajectory(0);
        //Plan_Trajectory(-2);
      }

      if(state->PLANNER_PF){

	printf("staring potential field planner\n");

	ClearWayPoints();

	double qf[4]={90.0,30.0,30.0,30.0};

#ifdef PF_DEBUG
	double potential;
	double torque[4];

	double val;

	qa[0] = qa[0];
	qa[1] = qa[1];
	qa[2] = qa[2];
	qa[3] = qa[3];

       	
	val = pf_objfun(qa,qf,xf,OBS,torque,0);
#endif


#ifndef PF_DEBUG
	
	gradientDescent(qa,qf,xf,OBS,1,0,0);
	
	printf("Planning trajectory\n");
	Plan_Trajectory(0);
	Plan_Trajectory(-2);

      
#endif

      }
      if(state->PLANNER_PRM && road_map != NULL){

       
	printf("staring planner for PRM\n");
	
	ClearWayPoints();
	
	double qf[4]={90,30,30,30};
		
	printf("Querying road maps....\n");
	PRM_Planner(qa,qf,xf,OBS);
	
	printf("Interpolating cubic splines\n");
	Plan_Trajectory(0);
	Plan_Trajectory(-2);
	
      }
      else{
	printf("***Construct roadmap***\n");
      }
      
      
    }
    else if(0==strcmp ("but11", name)){ 
	//Play 
	pthread_mutex_lock(&play_mutex);
	PLAY = 1;
	pthread_mutex_unlock(&play_mutex);
    }
    else if(0==strcmp ("but12", name)){ 
	//Stop
      pthread_mutex_lock(&DMM_mutex);
      state->DMM_STATE = 0;
      pthread_mutex_unlock(&DMM_mutex);

	pthread_mutex_lock(&play_mutex);
	PLAY = 0;
	pthread_mutex_unlock(&play_mutex);
    }

    else if(0==strcmp ("but13", name)){


      //CreateObsList(state);
 
      printf("Creating roadmaps ...\n");

      int add_points;
      printf("started graph_create\n");
      road_map = graph_create(); 
      printf("finished graph_create\n");
      printf("started graph_grid_points\n");
      add_points        = graph_grid_points(road_map,OBS);
      printf("finished graph_grid_points\n");
      printf("started random_points\n");
      random_points(road_map,OBS,add_points);
      printf("finished random_points\n");
      printf("started get_edges\n");
      get_edges(road_map,OBS);
      printf("finished get_edges\n");

      printf("Roadmap construction complete\n");
 
      double q0[4] = {0,90,0,0};
      printf("started find_closest_point\n");
      closest_qi = find_closest_point(road_map,q0,OBS);
      printf("finished find_closest_point\n");

      printf("started Dijkstra\n");
      Dijkstra(road_map,closest_qi);
      printf("finished Dijkstra\n");

      Graph_created = 1;

    }

    else if(0==strcmp ("but14", name)){

      obsList_t oList;

      int * ptr = NULL;


      unsigned char count = 0;

      if(cyl_temp_created){


	
	gettimeofday(&rec_start_time, NULL);
	
	getMatchLocations(state->snapshot_decimate,state->template_cylinder_decimate,ptr,&oList,TOLERANCE_CYL);

	gettimeofday(&cur_time, NULL);
      
	time = (cur_time.tv_sec - rec_start_time.tv_sec) +
	  1.0E-6*((double) (cur_time.tv_usec - rec_start_time.tv_usec));
      
	
	
	printf("Number of cylinders = %d\n",oList.n_obs);

	printf("Template matching time (cylinder) = %f\n",time);

	count = oList.n_obs;

	for(int i=1;i<=oList.n_obs;i++){
	  int x = getX_obsList(&oList,i) * DECIMATE;
	  int y = getY_obsList(&oList,i) * DECIMATE;

	  double width  = state->snapshot->width;
	  double height = state->snapshot->height;

	  double scale = 2.0/width;

	  double gy    = -(x - width/2)*scale;
	  double gx    = -(y - height + height/2)*scale;

	  
	  printf("x,y = %d %d, gx = %f, gy = %f\n",x,y,gx,gy);

#ifndef TEMP_DEBUG
	  double p_pos[2];
	  double w_pos[2];
	  
	  p_pos[0] = gx;
	  p_pos[1] = gy;
	  
	  affine_calibration_p2w(p_pos,w_pos);

	  state->Cpos[i-1].x = w_pos[0];
	  state->Cpos[i-1].y = w_pos[1];

	  

	  if( fabs(w_pos[0]) > 32  || fabs(w_pos[1]) > 32 ){
	    count= count-1;
	  }
	  else{
	    state->Cpos[i-1].x = w_pos[0];
	    state->Cpos[i-1].y = w_pos[1];
	  }
	  
#endif
	}

	state->num_cyl     = count;
	
      }
      

    }

    else if(0==strcmp ("but15", name)){

      obsList_t oList;

      int * ptr = NULL;

      
      unsigned char count = 0;

      if(ann_temp_created){
	
	gettimeofday(&rec_start_time, NULL);

	getMatchLocations(state->snapshot_decimate,state->template_annulus_decimate,ptr,&oList,TOLERANCE_ANN);
       
	
	gettimeofday(&cur_time, NULL);
	
	time = (cur_time.tv_sec - rec_start_time.tv_sec) +
	  1.0E-6*((double) (cur_time.tv_usec - rec_start_time.tv_usec));
	

	printf("Number of annuli = %d\n",oList.n_obs);

	printf("Template matching time (annulus) = %f\n",time);

	count = oList.n_obs;

	for(int i=1;i<=oList.n_obs;i++){
	  int x = getX_obsList(&oList,i) * DECIMATE;
	  int y = getY_obsList(&oList,i) * DECIMATE;

	  double width  = state->snapshot->width;
	  double height = state->snapshot->height;

	  double scale = 2.0/width;

	  double gy    = -(x - width/2)*scale;
	  double gx    = -(y - height + height/2)*scale;
	  
	  printf("x,y = %d %d, gx = %f, gy = %f\n",x,y,gx,gy);


#ifndef TEMP_DEBUG
	    double p_pos[2];
	    double w_pos[2];
	    
	    p_pos[0] = gx;
	    p_pos[1] = gy;
	    
	    affine_calibration_p2w(p_pos,w_pos);
	  

	    if( fabs(w_pos[0]) > 32  || fabs(w_pos[1]) > 32 ){
	      count= count-1;
	    }
	    else{
	      state->Apos[i-1].x = w_pos[0];
	      state->Apos[i-1].y = w_pos[1];
	    }
#endif
	  
	}

	state->num_ann  =  count;
	

      }

      

    }
    else if(0==strcmp ("but16", name)){
      

      //Create obstacle list

      if(OBS!=NULL){

	obstacle_t *ptr1 = OBS;
	obstacle_t *ptr2;
	
	while(ptr1->next != NULL){
	  ptr2 = ptr1->next;
	  free(ptr1);
	  ptr1 = ptr2;
	}

	free(ptr1);

	printf("Cleared obstacle list\n");
      }
     
      CreateObsList(state);

      obstacle_t *ptr = OBS;
      
      while(ptr){
	printf("obstacle created\n");
	ptr = ptr->next;
      }
      
 
    }

    else if(0==strcmp ("but17", name)){
      pthread_mutex_lock(&DMM_mutex);
      state->DMM_STATE = 1;
      pthread_mutex_unlock(&DMM_mutex);

      printf("DMM state set to 1\n");

    }


    else if(0==strcmp ("cb1", name)){ 
	//Inverse
	pthread_mutex_lock(&inverse_mutex);
        INVERSE = pg_gb (pg, name);
	pthread_mutex_unlock(&inverse_mutex);
	//printf("Inverse = %d\n",INVERSE);
    }
    else if(0==strcmp ("cb2", name)){ 
	//Elbow up
	state->ELBOW_UP = pg_gb (pg, name);
    }
    else if(0==strcmp ("cb3", name)){ 
	//Reverse config
	state->REVERSE_CONFIG = pg_gb (pg, name);
    }
    else if(0==strcmp ("cb4", name)){ 
	//Teach and repeat mode
	state->TEACH = pg_gb (pg, name);
	
	if(state->TEACH){
	    state->PLANNER_PF  = 0;
	    state->PLANNER_PRM = 0;
	    pg_sb(pg,"cb5",0);
	    pg_sb(pg,"cb7",0);
	    
	}

    }
    else if(0==strcmp ("cb5", name)){ 
	//Planner PF
	state->PLANNER_PF = pg_gb (pg, name);

	if(state->PLANNER_PF){
	    state->TEACH       = 0;
	    state->PLANNER_PRM = 0;
	    pg_sb(pg,"cb4",0);
	    pg_sb(pg,"cb7",0);
	}


    }
    else if(0==strcmp ("cb7", name)){ 
	//Planner PRM
	state->PLANNER_PRM = pg_gb (pg, name);

	if(state->PLANNER_PRM){
	    state->TEACH = 0;
	    state->PLANNER_PF = 0;
	    pg_sb(pg,"cb4",0);
	    pg_sb(pg,"cb5",0);
	}


    }

    else if(0==strcmp ("cb6", name)){ 
	//Planner
	state->GHOST = pg_gb (pg, name);
	
    }
    else if(0==strcmp ("db4", name)){ 
	//Inverse
	state->Z      = pg_gd_boxes (pg, name);
	planner_xg[2] = state->Z;
    }
    else if(0==strcmp ("db5", name)){ 
	//Inverse
	state->PITCH = pg_gd_boxes (pg, name);
    }
    else
	printf ("%s changed\n", name);
    

}



//Handle mouse clicks
int mouse_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_mouse_event_t *mouse)
{
    gui_state_t *state = vxeh->impl;

    // vx_camera_pos_t contains camera location, field of view, etc
    // vx_mouse_event_t contains scroll, x/y, and button click events

    static unsigned char first_vertex = 1;
    
    if(template_annulus){

	if ((mouse->button_mask & VX_BUTTON1_MASK)){
	    
	    vx_ray3_t ray;
	    vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);
	    
	    double ground[3];
	    vx_ray3_intersect_xy (&ray, 0, ground);
	    
	 
	    if(first_vertex){
		state->temp_ann_start[0] = ground[0];
		state->temp_ann_start[1] = ground[1];
		state->temp_ann_start[2] = mouse->x;
		state->temp_ann_start[3] = mouse->y;
		
		first_vertex = 0;

		printf ("Annulus template start: [%8.3f, %8.3f]\n",
		    mouse->x, mouse->y);

	    }
	    

	    state->temp_ann_stop[0] = ground[0];
	    state->temp_ann_stop[1] = ground[1];
	    state->temp_ann_stop[2] = mouse->x;
	    state->temp_ann_stop[3] = mouse->y;

	    printf ("Annulus template end: [%8.3f, %8.3f]\n",
		    mouse->x, mouse->y);
		
	}
    }
    else if(template_cylinder){

	if ((mouse->button_mask & VX_BUTTON1_MASK)){
	    
	    vx_ray3_t ray;
	    vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);
	    
	    double ground[3];
	    vx_ray3_intersect_xy (&ray, 0, ground);
	    
	 
	    if(first_vertex){
		state->temp_cyl_start[0] = ground[0];
		state->temp_cyl_start[1] = ground[1];
		state->temp_cyl_start[2] = mouse->x;
		state->temp_cyl_start[3] = mouse->y;
		
		first_vertex = 0;

		printf ("Cylinder template start: [%8.3f, %8.3f]\n",
		    mouse->x, mouse->y);

	    }
	    

	    state->temp_cyl_stop[0] = ground[0];
	    state->temp_cyl_stop[1] = ground[1];
	    state->temp_cyl_stop[2] = mouse->x;
	    state->temp_cyl_stop[3] = mouse->y;

	    printf ("Cylinder template end: [%8.3f, %8.3f]\n",
		    mouse->x, mouse->y);
		
	}
    }
    else
	first_vertex = 1;
    


    if ((mouse->button_mask & VX_BUTTON1_MASK) &&
        !(state->last_mouse_event.button_mask & VX_BUTTON1_MASK)) {

        vx_ray3_t ray;
        vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);

        double ground[3];
        vx_ray3_intersect_xy (&ray, 0, ground);

        printf ("Mouse clicked at coords: [%8.3f, %8.3f]  Ground clicked at coords: [%6.3f, %6.3f]\n",
                mouse->x, mouse->y, ground[0], ground[1]);

       

	if(state->calib_complete){
	  double p_goal[2];
	  double w_goal[2];
	  
	  p_goal[0] = ground[0];
	  p_goal[1] = ground[1];
	  
	  affine_calibration_p2w(p_goal,w_goal);
	  
	  printf("world position = x = %f, y =%f\n",w_goal[0],w_goal[1]);
	  
	}


	//Get calibration points
	if( state->calib_state > 0 && state->calib_state <= 5 && !state->calib_complete ){
	  
	  printf("calibration point %d\n",state->calib_state);
	  
	  state->calib_pts[state->calib_state-1].pixel[0] = ground[0];
	    state->calib_pts[state->calib_state-1].pixel[1] = ground[1];
	    state->calib_state = state->calib_state+1; 

	    
	}

	//Get cylinder co-ordinates
	if(state->num_cyl < 20 && state->draw_cyl == 1){

	    double p_goal[2];
	    double w_goal[2];

	    p_goal[0] = ground[0];
	    p_goal[1] = ground[1];

	    affine_calibration_p2w(p_goal,w_goal);
	    
	    state->Cpos[state->num_cyl].x = w_goal[0];
	    state->Cpos[state->num_cyl].y = w_goal[1];
	    state->draw_cyl               = 0;
	    state->num_cyl++;
	}

	//Get annulus co-ordinates
	if(state->num_ann < 20 && state->draw_ann == 1 ){
	    
	    double p_goal[2];
	    double w_goal[2];

	    p_goal[0] = ground[0];
	    p_goal[1] = ground[1];

	    affine_calibration_p2w(p_goal,w_goal);

	  state->Apos[state->num_ann].x = w_goal[0];
	  state->Apos[state->num_ann].y = w_goal[1];
	  state->draw_ann               = 0;
	  state->num_ann++;
	}

	pthread_mutex_lock(&inverse_mutex);
	if(INVERSE){
	    pthread_mutex_unlock(&inverse_mutex);
	    
	    inverse_input_p[0] = ground[0];
	    inverse_input_p[1] = ground[1];
	    
	    affine_calibration_p2w(inverse_input_p,inverse_input_w);
	    
	    //printf("move to p %lf,%lf w  %lf,%lf,%lf\n\n",ground[0],ground[1],inverse_input_w[0],inverse_input_w[1],state->Z);
	    
	    
	    int pos;
	    
	    if(state->ELBOW_UP == 0 && state->REVERSE_CONFIG==0)
		pos = 2;
	    
	    if(state->ELBOW_UP == 0 && state->REVERSE_CONFIG==1)
		pos = 4;
	    
	    
	    if(state->ELBOW_UP == 1 && state->REVERSE_CONFIG==0)
		pos = 1;
	    
	    
	    if(state->ELBOW_UP == 1 && state->REVERSE_CONFIG==1)
		pos = 3;
	    
	    double *q = inv_kin(inverse_input_w[0],inverse_input_w[1],
				state->Z,
				state->PITCH,
				pos,
				prev_servo);
	    
	    if(q[0]>180)
		q[0] = 180;
	    
	    if(q[0]<-180)
		q[0] = -180;
	    
	    
	    //printf("angles = %lf,%lf,%lf,%lf\n",q[0],q[1],q[2],q[3]);
	    
	    pthread_mutex_lock(&inverse_mutex);
	    inverse_q[0] = q[0];
	    inverse_q[1] = q[1];
	    inverse_q[2] = q[2];
	    inverse_q[3] = q[3];
	    pthread_mutex_unlock(&inverse_mutex);
	    
	    
	}
	else{
	    pthread_mutex_unlock(&inverse_mutex);
	}
	

	if(state->PLANNER_PRM || state->PLANNER_PF ){

	    double p_goal[2];
	    double w_goal[2];

	    p_goal[0] = ground[0];
	    p_goal[1] = ground[1];

	    affine_calibration_p2w(p_goal,w_goal);

	    planner_xg[0] = w_goal[0];
	    planner_xg[1] = w_goal[1];

	}

    }

    // store previous mouse event to see if the user *just* clicked or released
    state->last_mouse_event = *mouse;
    
    return 0;
}

int
key_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_key_event_t *key)
{
    //state_t *state = vxeh->impl;

    if(!key->released && key->key_code == 0x0061){
	extract_annulus = 0;
	if(!template_annulus)
	    template_annulus = 1;
    }
    else if(key->released && key->key_code == 0x0061){
	template_annulus = 0;	
	//Extract template
	extract_annulus = 1;
	ann_temp_created = 0;
	printf("Extracting template for annuli\n");
    }
    else if(!key->released && key->key_code == 0x0063){
	extract_cylinder = 0;
	if(!template_cylinder)
	    template_cylinder = 1;
    }
    else if(key->released && key->key_code == 0x0063){
	template_cylinder = 0;	
	extract_cylinder = 1;
	cyl_temp_created = 0;
	printf("Extracting template for cylinders\n");
    }


    return 0;
}

int
touch_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_touch_event_t *mouse)
{
    return 0; // Does nothing
}

// === Your code goes here ================================================
// The render loop handles your visualization updates. It is the function run
// by the animate_thread. It periodically renders the contents on the
// vx world contained by state
void *
animate_thread (void *data)
{
    const int fps = 60;
    gui_state_t *state = data;

    // Set up the imagesource
    image_source_t *isrc = image_source_open (state->img_url);

    // Work space calibration not done.
    state->calib_state    = 0;
    state->calib_complete = 0;

    vx_object_t *vxo_sphere[4];
    vx_object_t *vxo_cylinder[20];
    vx_object_t *vxo_annulus[20];
    vx_object_t *vxo_arm0;
    vx_object_t *vxo_prm[700];
   

    pthread_mutex_lock(&servo_mutex);
    user_cmds.speed      = 0.05;
    user_cmds.torque     = 0.35;
    user_cmds.servo[0]   = 0;
    user_cmds.servo[1]   = 0;
    user_cmds.servo[2]   = 0;
    user_cmds.servo[3]   = 0;
    user_cmds.play_speed = 5;
    pthread_mutex_unlock(&servo_mutex);

        
    if (isrc == NULL)
        printf ("Error opening device.\n");
    else {
        // Print out possible formats. If no format was specified in the
        // url, then format 0 is picked by default.
        // e.g. of setting the format parameter to format 2:
        //
        // --url=dc1394://bd91098db0as9?fidx=2
        for (int i = 0; i < isrc->num_formats (isrc); i++) {
            image_source_format_t ifmt;
            isrc->get_format (isrc, i, &ifmt);
            printf ("%3d: %4d x %4d (%s)\n",
                    i, ifmt.width, ifmt.height, ifmt.format);
        }
        isrc->start (isrc);
    }

    // Continue running until we are signaled otherwise. This happens
    // when the window is closed/Ctrl+C is received.

    while (state->running) {
	
#ifdef CAMERA
        // Get the most recent camera frame and render it to screen.
        if (isrc != NULL) {
            image_source_data_t *frmd = calloc (1, sizeof(*frmd));
            int res = isrc->get_frame (isrc, frmd);
            if (res < 0)
                printf ("get_frame fail: %d\n", res);
            else {
                // Handle frame
                image_u32_t *im = image_convert_u32 (frmd);
                if (im != NULL) {
                    vx_object_t *vim = vxo_image_from_u32(im,
                                                          VXO_IMAGE_FLIPY,
                                                          VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);
		    
                    // render the image centered at the origin and at a normalized scale of +/-1 unit in x-dir
                    const double scale = 2./im->width;
                    vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image"),
                                        vxo_chain (vxo_mat_rotate_z(-90*3.142/180),vxo_mat_scale3 (scale, scale, 1.0),
                                                   vxo_mat_translate3 (-im->width/2., -im->height/2., 0.),
                                                   vim));
                    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "image"));


		    double t_width_cyl = 0;

		    if(extract_cylinder){

			
			double t_width_g  = fabs(state->temp_cyl_start[0] - state->temp_cyl_stop[0]);
			double t_height_g = fabs(state->temp_cyl_start[1] - state->temp_cyl_stop[1]);
			
			double t_width_p  = (t_width_g/scale);
			double t_height_p = (t_height_g/scale);
			t_width_cyl       = t_width_p;
			
			//double x = (state->temp_ann_start[0]/scale + im->width/2);
			//double y = fabs(im->height - (state->temp_ann_start[1]/scale + im->height/2));
			
			double x = (-state->temp_cyl_start[1]/scale + im->width/2);
			double y = fabs(im->height - (state->temp_cyl_start[0]/scale + im->height/2));
						
			if(!cyl_temp_created){

			    state->template_cylinder     =  image_u32_create((int)t_width_p,(int)t_height_p);

			    
			    printf("template created\n");
			    cyl_temp_created = 1;
			
			    for(int i=0;i<(int)t_height_p;i++){
				for(int j=0;j<(int)t_width_p;j++){	
				    state->template_cylinder->buf[i*state->template_cylinder->stride + ((int)t_width_p - 1 - j)] = im->buf[((int)(y)+j)*im->stride + ((int)(x)+i)]; 
				    //printf("%d %d template extracting\n",(int)(y)+i,(int)(x)+j);
				}
			    }

			  


			    state->template_cylinder_decimate = image_util_u32_decimate(state->template_cylinder,DECIMATE);
			    
			}
			vx_object_t *vim2 = vxo_image_from_u32(state->template_cylinder,VXO_IMAGE_FLIPY,
							       VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);
			
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image3"),vxo_pix_coords(VX_ORIGIN_BOTTOM_LEFT,vxo_chain(vxo_mat_translate3(0,0,0),vim2)));
			
			vx_buffer_swap (vx_world_get_buffer (state->vxworld, "image3"));
		    }



		    
		    if(extract_annulus){

		      			
			
			double t_width_g  = fabs(state->temp_ann_start[0] - state->temp_ann_stop[0]);
			double t_height_g = fabs(state->temp_ann_start[1] - state->temp_ann_stop[1]);
			
			double t_width_p  = (t_width_g/scale);
			double t_height_p = (t_height_g/scale);
			
			//double x = (state->temp_ann_start[0]/scale + im->width/2);
			//double y = fabs(im->height - (state->temp_ann_start[1]/scale + im->height/2));
			
			double x = (-state->temp_ann_start[1]/scale + im->width/2);
			double y = fabs(im->height - (state->temp_ann_start[0]/scale + im->height/2));
			
			
			//printf("temp_width_g = %f,temp_height_g = %f\n",t_width_g,t_height_g);	
			//printf("temp_width_p = %f,temp_height_p = %f,x,y = %f,%f\n",t_width_p,t_height_p,x,y);
			//printf("image width =%d,height = %d\n",im->width,im->height);
			
			
			if(!ann_temp_created){
	       		

			  state->snapshot          = image_convert_u32 (frmd); 

			  state->snapshot_decimate = image_util_u32_decimate(state->snapshot,DECIMATE);
			  
			  
			  int temp_width  = (int)t_width_p;
			  int temp_height = (int)t_height_p;
			  
			  state->template_annulus = image_u32_create(temp_width,temp_height);
			  
			 
			  printf("template created\n");
			  ann_temp_created = 1;
			  
			  for(int i=0;i<(int)t_height_p;i++){
			    for(int j=0;j<(int)t_width_p;j++){	
			      state->template_annulus->buf[i*state->template_annulus->stride + ((int)t_width_p - 1 - j)] = im->buf[((int)(y)+j)*im->stride + ((int)(x)+i)]; 
			      //printf("%d %d template extracting\n",(int)(y)+i,(int)(x)+j);
			    }
			  }
			  
			  state->template_annulus_decimate = image_util_u32_decimate(state->template_annulus,DECIMATE);
			
			 
			  
			}
			
			vx_object_t *vim2 = vxo_image_from_u32(state->template_annulus,VXO_IMAGE_FLIPY,
							       VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);
			
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image2"),vxo_pix_coords(VX_ORIGIN_BOTTOM_LEFT,vxo_chain(vxo_mat_translate3(t_width_cyl,0,0),vim2)));
			
			vx_buffer_swap (vx_world_get_buffer (state->vxworld, "image2"));
			
			
		    }
		    
		}
		
		image_u32_destroy (im);
	    }
	    
	    fflush (stdout);
	    isrc->release_frame (isrc, frmd);
	}
#endif
    
	#ifndef CAMERA

	vx_object_t *vxo_base;

	
	vxo_base  = vxo_chain (vxo_mat_scale3 (0.7,0.7,0.0005),
				   vxo_box (vxo_mesh_style (vx_gray)));
	
	vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "base"), vxo_base);
	vx_buffer_swap (vx_world_get_buffer (state->vxworld, "base"));

	#endif

        // Creates a blue box and applies a series of rigid body transformations
        // to it. A vxo_chain applies its arguments sequentially. In this case,
        // then, we rotate our coordinate frame by rad radians, as determined
        // by the current time above. Then, the origin of our coordinate frame
        // is translated 0 meters along its X-axis and 0.5 meters along its
        // Y-axis. Finally, a 0.1 x 0.1 x 0.1 cube (or box) is rendered centered at the
        // origin, and is rendered with the blue mesh style, meaning it has
        // solid, blue sides.

	//Draw calibration points
	for(int i=0;i < state->calib_state;i++){
	    vxo_sphere[i] = vxo_chain (vxo_mat_translate2 (state->calib_pts[i].pixel[0],state->calib_pts[i].pixel[1]),
				       vxo_mat_scale (0.005),
				       vxo_sphere (vxo_mesh_style (vx_blue)));
	
	    vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "sphere"), vxo_sphere[i]);
	    
	}
	
	


	// Draw cylinder
	for(int i=0;i<state->num_cyl;i++){
	  vxo_cylinder[i] = vxo_chain (vxo_mat_translate2 (origin_p[0],origin_p[1]),vxo_mat_scale (wp_scaling),vxo_mat_translate3(0,0,CYL_H/2),
					 vxo_chain(vxo_mat_translate2 (state->Cpos[i].x,state-> Cpos[i].y),
						   vxo_mat_scale3 (CYL_R*2,CYL_R*2,CYL_H), 
						   vxo_cylinder(vxo_mesh_style (vx_white),vxo_lines_style(vx_black,2))));
	  
	  vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "cylinder"), vxo_cylinder[i]);
	  

	  if(i==0){
	    float points[24] = { state->Cpos[i].x-5,state->Cpos[i].y+5,0.01,
	                         state->Cpos[i].x+5,state->Cpos[i].y+5,0.01,
                                 state->Cpos[i].x+5,state->Cpos[i].y+5,0.01,
				 state->Cpos[i].x+5,state->Cpos[i].y-5,0.01,
				 state->Cpos[i].x+5,state->Cpos[i].y-5,0.01,
				 state->Cpos[i].x-5,state->Cpos[i].y-5,0.01,
				 state->Cpos[i].x-5,state->Cpos[i].y-5,0.01,
				 state->Cpos[i].x-5,state->Cpos[i].y+5,0.01
	                       };

	    int npoints = 8;
	    vx_resc_t *verts = vx_resc_copyf(points, npoints*3);
	    vx_buffer_add_back(vx_world_get_buffer (state->vxworld, "cyl_best_template"),
			       vxo_chain(vxo_mat_translate2(origin_p[0],origin_p[1]),vxo_mat_scale(wp_scaling),vxo_lines(verts, npoints, GL_LINES, vxo_points_style(vx_blue, 2.0f)))); 
	    
	    
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "cyl_best_template"));

	  }


	  
	}

	// Draw annulus
	for(int i=0;i<state->num_ann;i++){
	  vxo_annulus[i] = vxo_chain (vxo_mat_translate2 (origin_p[0],origin_p[1]),vxo_mat_scale(wp_scaling),vxo_mat_translate3(0,0,ANN_H/2),
					vxo_chain(vxo_mat_translate2 (state->Apos[i].x,state-> Apos[i].y),
						  vxo_mat_scale3 (ANN_RO*2,ANN_RO*2,ANN_H), 
						  vxo_cylinder(vxo_mesh_style (vx_black),vxo_lines_style(vx_gray,2))));
	    
	  vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "annulus"), vxo_annulus[i]);

	  if(i==0){
	    float points[24] = { state->Apos[i].x-5,state->Apos[i].y+5,0.01,
	                         state->Apos[i].x+5,state->Apos[i].y+5,0.01,
                                 state->Apos[i].x+5,state->Apos[i].y+5,0.01,
				 state->Apos[i].x+5,state->Apos[i].y-5,0.01,
				 state->Apos[i].x+5,state->Apos[i].y-5,0.01,
				 state->Apos[i].x-5,state->Apos[i].y-5,0.01,
				 state->Apos[i].x-5,state->Apos[i].y-5,0.01,
				 state->Apos[i].x-5,state->Apos[i].y+5,0.01
	                       };

	    int npoints = 8;
	    vx_resc_t *verts = vx_resc_copyf(points, npoints*3);
	    vx_buffer_add_back(vx_world_get_buffer (state->vxworld, "ann_best_template"),
			       vxo_chain(vxo_mat_translate2(origin_p[0],origin_p[1]),vxo_mat_scale(wp_scaling),vxo_lines(verts, npoints, GL_LINES, vxo_points_style(vx_blue, 2.0f)))); 
	    
	    
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_best_template"));

	    
	  }

	    
	}

	int errors[4]      = {0,0,0,0};
	double angles[4]   = {0,0,0,0};
	float arm_color[4] = {0,0,0,0};
	float alpha        = 1;

	//printf("INVERSE = %d\n",INVERSE);

	if(state->GHOST == 0){
	  pthread_mutex_lock(&fdb_mutex);
	  angles[0] = servo_fdb.servo[0];
	  angles[1] = servo_fdb.servo[1];
	  angles[2] = servo_fdb.servo[2];
	  angles[3] = servo_fdb.servo[3];
	  pthread_mutex_unlock(&fdb_mutex);
	}
	else{
	  pthread_mutex_lock(&servo_mutex);
	  angles[0] = user_cmds.servo[0];
	  angles[1] = user_cmds.servo[1];
	  angles[2] = user_cmds.servo[2];
	  angles[3] = user_cmds.servo[3];
	  pthread_mutex_unlock(&servo_mutex);
	}
	
	// Draw rexarm
	if(state->calib_complete){
	    vxo_arm0 = rexarm_vxo_arm(errors,angles,arm_color,alpha);
	    vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "arm"),vxo_arm0);
	}
       	
        // Draw a default set of coordinate axes
        vx_object_t *vxo_axe = vxo_chain (vxo_mat_translate2 (origin_p[0],origin_p[1]),vxo_mat_scale (0.1), // 10 cm axes
                                          vxo_axes ());

        vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "axes"), vxo_axe);

	if(state->calib_complete){
	    vx_object_t *vxo_FP = vxo_chain (vxo_mat_translate2(origin_p[0],origin_p[1]),
					     vxo_mat_scale(wp_scaling), // 10 cm axes
					     vxo_chain(vxo_mat_translate3(FP_gui[0],FP_gui[1],FP_gui[2]),vxo_mat_scale(0.5),
						       vxo_sphere(vxo_mesh_style(vx_red))));
	    
	    vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "FP"), vxo_FP);
	    
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "FP"));
	}

	if(template_annulus){
	    float points[24] = {state->temp_ann_start[0],state->temp_ann_start[1],0.01,
				state->temp_ann_stop[0],state->temp_ann_start[1],0.01,
				state->temp_ann_stop[0],state->temp_ann_start[1],0.01,
				state->temp_ann_stop[0],state->temp_ann_stop[1],0.01,
	                        state->temp_ann_stop[0],state->temp_ann_stop[1],0.01,
				state->temp_ann_start[0],state->temp_ann_stop[1],0.01,
	                        state->temp_ann_start[0],state->temp_ann_stop[1],0.01,
	                        state->temp_ann_start[0],state->temp_ann_start[1],0.01};
	    int npoints = 8;
	    vx_resc_t *verts = vx_resc_copyf(points, npoints*3);
	    vx_buffer_add_back(vx_world_get_buffer (state->vxworld, "ann_template"),
			       vxo_lines(verts, npoints, GL_LINES, vxo_points_style(vx_red, 2.0f))); 
	    
	    
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_template"));
	}
	else if(template_cylinder){
	    float points[24] = {state->temp_cyl_start[0],state->temp_cyl_start[1],0.01,
				state->temp_cyl_stop[0],state->temp_cyl_start[1],0.01,
				state->temp_cyl_stop[0],state->temp_cyl_start[1],0.01,
				state->temp_cyl_stop[0],state->temp_cyl_stop[1],0.01,
	                        state->temp_cyl_stop[0],state->temp_cyl_stop[1],0.01,
				state->temp_cyl_start[0],state->temp_cyl_stop[1],0.01,
	                        state->temp_cyl_start[0],state->temp_cyl_stop[1],0.01,
	                        state->temp_cyl_start[0],state->temp_cyl_start[1],0.01};
	    int npoints = 8;
	    vx_resc_t *verts = vx_resc_copyf(points, npoints*3);
	    vx_buffer_add_back(vx_world_get_buffer (state->vxworld, "ann_cylinder"),
			       vxo_lines(verts, npoints, GL_LINES, vxo_points_style(vx_green, 2.0f))); 
	    
	    
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_cylinder"));
	}
	else{
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_template"));
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ann_cylinder"));
	}

	if(Graph_created){

	    for(int i=0;i<road_map->n_nodes;i++){

		double *prm_pos;

		prm_pos = tip_pos(road_map->nodes[i].q[0],road_map->nodes[i].q[1],
				  road_map->nodes[i].q[2],road_map->nodes[i].q[3],4);

		vxo_prm[i]   = vxo_chain (vxo_mat_translate2(origin_p[0],origin_p[1]),vxo_mat_scale(wp_scaling),
					  vxo_chain(vxo_mat_translate3(prm_pos[0],prm_pos[1],prm_pos[2]),vxo_mat_scale(0.5),vxo_sphere (vxo_mesh_style (vx_red))));
		
		vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "prm"), vxo_prm[i]);

		free(prm_pos);
	    }


	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "prm"));
	}

	
        // Now, we update both buffers
	if(state->calib_state > 0){
	    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "sphere"));
	}
        vx_buffer_swap (vx_world_get_buffer (state->vxworld, "axes"));

	if(state->num_cyl>0)
	  vx_buffer_swap (vx_world_get_buffer (state->vxworld, "cylinder"));

	if(state->num_ann>0)
	  vx_buffer_swap (vx_world_get_buffer (state->vxworld, "annulus"));


	if(state->click_clr){
		vx_buffer_swap (vx_world_get_buffer (state->vxworld, "cylinder"));
		vx_buffer_swap (vx_world_get_buffer (state->vxworld, "annulus"));
		
		state->click_clr = false;
	}

	vx_buffer_swap (vx_world_get_buffer (state->vxworld, "arm"));
	
      
        usleep (1000000/fps);



    }

    if (isrc != NULL)
        isrc->stop (isrc);

    return NULL;
}

gui_state_t *
state_create (void)
{
    gui_state_t *state = calloc (1, sizeof(*state));

    state->vxworld = vx_world_create ();
    state->vxeh = calloc (1, sizeof(*state->vxeh));
    state->vxeh->key_event = key_event;
    state->vxeh->mouse_event = mouse_event;
    state->vxeh->touch_event = touch_event;
    state->vxeh->dispatch_order = 100;
    state->vxeh->impl = state; // this gets passed to events, so store useful struct here!

    state->vxapp.display_started = rob550_default_display_started;
    state->vxapp.display_finished = rob550_default_display_finished;
    state->vxapp.impl = rob550_default_implementation_create (state->vxworld, state->vxeh);

    state->running = 1;

    state->calib_pts[0].world[0] = CALIB_X_1;
    state->calib_pts[0].world[1] = CALIB_Y_1;

    state->calib_pts[1].world[0] = CALIB_X_2;
    state->calib_pts[1].world[1] = CALIB_Y_2;

    state->calib_pts[2].world[0] = CALIB_X_3;
    state->calib_pts[2].world[1] = CALIB_Y_3;
	
    state->calib_pts[3].world[0] = CALIB_X_4;
    state->calib_pts[3].world[1] = CALIB_Y_4;
  
    state->num_cyl = 0;
    state->num_ann = 0;

    state->draw_cyl = 0;
    state->draw_ann = 0;


    state->Z        = 1;
    state->PITCH    = 0;

    state->ELBOW_UP       = 1;
    state->REVERSE_CONFIG = 0;

    state->TEACH   = 0;
    state->PLANNER_PF = 0;
    state->PLANNER_PRM = 0;
    state->GHOST   = 0;
    //Initialize waypoint and trajectory structs

    
    return state;
}

void CreateObsList(gui_state_t *state){

    
    obstacle_t * ptr;
    OBS = (obstacle_t *)calloc(1,sizeof(obstacle_t));
  
    ptr = OBS;
    
    ptr->obs_type = 1;
    ptr->height   = 6;
    ptr->x        = 0;
    ptr->y        = 1;
    ptr->radius_O = 2;
    ptr->radius_I = 0;
    ptr->next     = NULL;

    for(int i=0;i<state->num_cyl;i++){
	
	ptr->next = (obstacle_t *)calloc(1,sizeof(obstacle_t));
	
	ptr = ptr->next;

	ptr->obs_type = 1;
	ptr->height   = CYL_H;
	ptr->x        = state->Cpos[i].x;
	ptr->y        = state->Cpos[i].y;
	ptr->radius_O = CYL_R;
	ptr->radius_I = 0;
	ptr->next     = NULL;


    }

    
    for(int i=0;i<state->num_ann;i++){

	ptr->next = (obstacle_t *)calloc(1,sizeof(obstacle_t));

	ptr = ptr->next;

	ptr->obs_type = 2;
	ptr->height   = ANN_H;
	ptr->x        = state->Apos[i].x;
	ptr->y        = state->Apos[i].y;
	ptr->radius_O = ANN_RO;
	ptr->radius_I = ANN_RI;
	ptr->next     = NULL;
	
	
    }


    ptr->next = (obstacle_t *)calloc(1,sizeof(obstacle_t));

    ptr = ptr->next;

    //First add table
    ptr->obs_type = 3;
    ptr->height   = 0.5;
    ptr->next     = NULL;
    
   
   
    ptr->next = (obstacle_t *)calloc(1,sizeof(obstacle_t));
    
    ptr = ptr->next;

    ptr->obs_type = 3;
    ptr->height   = 36;
    ptr->next     = NULL;


    
    
    
   

    
}


void
state_destroy (gui_state_t *state)
{
    if (!state)
        return;


    if(state->snapshot != NULL){
      image_u32_destroy (state->snapshot);
      image_u32_destroy (state->snapshot_decimate);
    }

    if(state->template_annulus != NULL){
      image_u32_destroy (state->template_annulus);
      image_u32_destroy (state->template_annulus_decimate);
    }

    if(state->template_cylinder != NULL){
      image_u32_destroy (state->template_cylinder);
      image_u32_destroy (state->template_cylinder_decimate);
    }

    free (state->vxeh);
    getopt_destroy (state->gopt);
    pg_destroy (state->pg);
    free (state);
}





#endif
