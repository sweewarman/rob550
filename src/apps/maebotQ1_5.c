#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <math.h>

// core api
#include "vx/vx.h"
#include "vx/vx_util.h"
#include "vx/vx_remote_display_source.h"
#include "vx/gtk/vx_gtk_display_source.h"

// drawables
#include "vx/vxo_drawables.h"

// common
#include "common/getopt.h"
#include "common/pg.h"
#include "common/zarray.h"

#include "math/gsl_util_eigen.h"

// imagesource
#include "imagesource/image_u32.h"
#include "imagesource/image_source.h"
#include "imagesource/image_convert.h"



#include "rob550_util.h"    // This is where a lot of the internals live

#define VXO_GRID_SIZE 0.25 // [m]

#define GOAL_RADIUS 0.10 // [m]

#define ELLIPSES 3

#define TRAJ_LEN 100

#define PLOT_ELLIPSE_DISTANCE 1

double dx = 0;

//float trajectory[3*100];

zarray_t *trajectory;
zarray_t *ellipse;

double dist_traveled = 0;

unsigned char draw_ellipse = 0;

double xyt_ellipse[3];


// It's good form for every application to keep its state in a struct.
typedef struct state state_t;
struct state {
    bool running;

    getopt_t        *gopt;
    parameter_gui_t *pg;

 

    unsigned char have_goal;

    double goal[2];

    double loc_x[100];
    double loc_y[100];

    // vx stuff
    vx_application_t    vxapp;
    vx_world_t         *vw;      // where vx objects are live
    vx_event_handler_t *vxeh; // for getting mouse, key, and touch events
    vx_mouse_event_t    last_mouse_event;

    // threads
    pthread_t animate_thread;

    // for accessing the arrays
    pthread_mutex_t mutex;
};


/*
void init_trajectory(){

    for(int i=0;i<300;i++){
	trajectory[i] = 0;
    }
}


void update_trajectory(double x,double y){

    for(int i=(100*3)-1;i>0;i--){
	trajectory[i] = trajectory[i-3]; 
	
    }

    trajectory[2] = 0;
    trajectory[1] = y;
    trajectory[0] = x;

    }*/

void Get_Trajectory(float *line, zarray_t * traj){

    int size_traj = zarray_size(traj);
    double xy[2];

    
    if(size_traj>0){
	for(int i=0;i<size_traj;i++){

	    if(i>=TRAJ_LEN)
		break;

	    
	    zarray_get(traj,(size_traj - 1) - i,xy);
	    
	    line[3*i + 0] = (float)xy[0];
	    line[3*i + 1] = (float)xy[1];
	    line[3*i + 2] = (float)0;
	    
	}

	for(int i=size_traj;i<TRAJ_LEN;i++){
	    line[3*i + 0] = (float)xy[0];
	    line[3*i + 1] = (float)xy[1];
	    line[3*i + 2] = 0.0;
	}

    }
    else{
	for(int i=0;i<TRAJ_LEN;i++){
	       
	    line[3*i + 0] = 0;
	    line[3*i + 1] = 0;
	    line[3*i + 2] = 0;
	    
	}

    }

}


// === Parameter listener =================================================
// This function is handed to the parameter gui (via a parameter listener)
// and handles events coming from the parameter gui. The parameter listener
// also holds a void* pointer to "impl", which can point to a struct holding
// state, etc if need be.
static void
my_param_changed (parameter_listener_t *pl, parameter_gui_t *pg, const char *name)
{
    if (0==strcmp ("sl1", name))
        printf ("sl1 = %f\n", pg_gd (pg, name));
    else if (0==strcmp ("sl2", name))
        printf ("sl2 = %d\n", pg_gi (pg, name));
    else if (0==strcmp ("cb1", name) || 0==strcmp ("cb2", name))
        printf ("%s = %d\n", name, pg_gb (pg, name));
    else
        printf ("%s changed\n", name);
}

static int
mouse_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_mouse_event_t *mouse)
{
    state_t *state = vxeh->impl;

    // vx_camera_pos_t contains camera location, field of view, etc
    // vx_mouse_event_t contains scroll, x/y, and button click events

    if ((mouse->button_mask & VX_BUTTON1_MASK) &&
        !(state->last_mouse_event.button_mask & VX_BUTTON1_MASK)) {

        vx_ray3_t ray;
        vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);

        double ground[3];
        vx_ray3_intersect_xy (&ray, 0, ground);

        printf ("Mouse clicked at coords: [%8.3f, %8.3f]  Ground clicked at coords: [%6.3f, %6.3f]\n",
                mouse->x, mouse->y, ground[0], ground[1]);
    }

    // store previous mouse event to see if the user *just* clicked or released
    state->last_mouse_event = *mouse;

    return 0;
}

static int
key_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_key_event_t *key)
{
    //state_t *state = vxeh->impl;
    //

    if(!key->released && key->key_code == VX_KEY_LEFT){
	dx = dx + 0.01;
    }

    return 0;

}

static int
touch_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_touch_event_t *mouse)
{
    return 0; // Does nothing
}


//Compute distance
static double dist(double x1,double y1,double x2, double y2){
    
    double d;

    d = sqrt( pow(x1-x2,2) + pow(y1-y2,2) );

    return d;
    
}


// === Your code goes here ================================================
// The render loop handles your visualization updates. It is the function run
// by the animate_thread. It periodically renders the contents on the
// vx world contained by state
void *
animate_thread (void *data)
{
    const int fps = 30;
    state_t *state = data;

    
    
    // Grid
    {
        vx_buffer_t *vb = vx_world_get_buffer (state->vw, "grid");
        vx_buffer_set_draw_order (vb, 0);
        vx_buffer_add_back (vb,
                            vxo_chain (vxo_mat_scale (VXO_GRID_SIZE),
                                       vxo_grid ()));
        vx_buffer_swap (vb);
    }

    // Axes
    {
        vx_buffer_t *vb = vx_world_get_buffer (state->vw, "axes");
        vx_buffer_set_draw_order (vb, 0);
        vx_buffer_add_back (vb,
                            vxo_chain (vxo_mat_scale3 (0.10, 0.10, 0.0),
                                       vxo_mat_translate3 (0.0, 0.0, -0.005),
                                       vxo_axes_styled (vxo_mesh_style (vx_red),
                                                        vxo_mesh_style (vx_green),
                                                        vxo_mesh_style (vx_black))));
        vx_buffer_swap (vb);
    }

    
    while (state->running) {
    
        {
            // Goal
            if (state->have_goal) {
                float color[4] = {0.0, 1.0, 0.0, 0.5};
                vx_buffer_t *vb = vx_world_get_buffer (state->vw, "goal");
                vx_buffer_set_draw_order (vb, -1);
                vx_buffer_add_back (vb,
                                    vxo_chain (vxo_mat_translate2 (state->goal[0], state->goal[1]),
                                               vxo_mat_scale (GOAL_RADIUS),
                                               vxo_circle (vxo_mesh_style (color))));
                vx_buffer_swap (vb);
            }
	    
            // Robot
            {

		

                vx_buffer_t *vb = vx_world_get_buffer (state->vw, "robot");
                vx_buffer_set_draw_order (vb, 1);
                enum {ROBOT_TYPE_TRIANGLE, ROBOT_TYPE_DALEK};
                vx_object_t *robot = NULL;
                switch (ROBOT_TYPE_TRIANGLE) {
                    case ROBOT_TYPE_DALEK: {
                        float line[6] = {0.0, 0.0, 0.151, 0.104, 0.0, 0.151};
                        robot = vxo_chain (vxo_lines (vx_resc_copyf (line, 6),
                                                      2,
                                                      GL_LINES,
                                                      vxo_lines_style (vx_red, 3.0f)),
                                           vxo_mat_scale3 (0.104, 0.104, 0.151),
                                           vxo_mat_translate3 (0.0, 0.0, 0.5),
                                           vxo_cylinder (vxo_mesh_style (vx_blue)));
                        break;
                    }
                    case ROBOT_TYPE_TRIANGLE:
                    default:
                        robot = vxo_chain (vxo_mat_scale (0.104),
                                           vxo_mat_scale3 (1, 0.5, 1),
                                           vxo_triangle (vxo_mesh_style (vx_blue)));
                        break;
                }


		//Set current pose angle
		double xyt[3] = {0,0,0.0};

		xyt[0] = xyt[0] + dx;

		
		vx_buffer_add_back (vb,
				    vxo_chain (
					       vxo_mat_from_xyt (xyt),
					       robot));

		double xy[2];

		int traj_size = zarray_size(trajectory);
		
	       
		if(traj_size)
		    zarray_get(trajectory,traj_size-1,xy);

		
		if(  fabs( xyt[0]*1e6 - xy[0]*1e6 ) > 1 || fabs( xyt[1]*1e6 - xy[1]*1e6) > 1 )    {
		    
		    dist_traveled += dist(xy[0],xy[1],xyt[0],xyt[1]);
		    
		    double new_xy[2] = {xyt[0],xyt[1]};

		    zarray_add(trajectory,new_xy);
		    
		    
		}
		

		if(dist_traveled > PLOT_ELLIPSE_DISTANCE){
		 
		    //update ellipse parameters
		    dist_traveled = 0;
		    draw_ellipse  = 1;

		    gsl_matrix *xy_cov = gsl_matrix_calloc(2,2);

		    //Extract the xy covariance submatrix
		    gsl_matrix_set(xy_cov,0,0,0.05);
		    gsl_matrix_set(xy_cov,0,1,0);
		    gsl_matrix_set(xy_cov,1,0,0);
		    gsl_matrix_set(xy_cov,1,1,0.001);

		    //Get the eigen decomposition
		    gslu_eigen* cov_ell = gslu_eigen_decomp_alloc(xy_cov);

		    //Get sqrt of D
		    double d11 = sqrt(gsl_vector_get(cov_ell->D,0));
		    double d22 = sqrt(gsl_vector_get(cov_ell->D,1));

		    gsl_matrix *sqD = gsl_matrix_calloc(2,2);

		    gsl_matrix_set(sqD,0,0,d11);
		    gsl_matrix_set(sqD,1,1,d22);
		    
		    //Multiply matrices V and sqD
		    gsl_matrix *Sig = gsl_matrix_calloc(2,2);

		    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,1.0, cov_ell->V, sqD,0.0, Sig);

		    //Transformation matrix to plot ellipse
		    double A_sig[16] = {Sig->data[0],Sig->data[1],0,xyt[0],
				        Sig->data[2],Sig->data[3],0,xyt[1],
				        0           ,0           ,1,0     ,
				        0           ,0           ,0,1      };
		    
		    
		    zarray_add(ellipse,A_sig);
		    
		    
		}

		
				
		vx_object_t *traj = NULL;

		float lines[3*TRAJ_LEN];
		Get_Trajectory(lines,trajectory);

		
		vx_buffer_t *vx_traj = vx_world_get_buffer (state->vw, "trajectory");

		traj = vxo_chain (vxo_lines (vx_resc_copyf (lines, 3*TRAJ_LEN),
					     TRAJ_LEN,
					     GL_LINES,
					     vxo_lines_style (vx_red, 3.0f)));
		
		
		vx_buffer_add_back (vx_traj,traj);
			
		vx_buffer_swap(vx_traj);
		
		
		
		if(draw_ellipse){

		    int size_ellipse = zarray_size(ellipse);
		    
		    
		    double A_ellipse[16];
		    
		    vx_buffer_t *vx_ellipse = vx_world_get_buffer (state->vw, "ellipse");

	       
		    for(int j=0;j<size_ellipse;j++){
			
						
			zarray_get(ellipse,j,A_ellipse);

			vx_buffer_add_back (vx_ellipse,
					    vxo_chain (
						vxo_mat_copy_from_doubles(A_ellipse),
						vxo_circle(vxo_mesh_style(vx_orange),
							   vxo_lines_style(vx_green, 3.0f))));
			
			
		    }

		    vx_buffer_swap(vx_ellipse);
		}
                
		
                vx_buffer_swap (vb);

		
            }
	    

	    

            // Robot Covariance
            // HINT: vxo_circle is what you want

            // Current Lidar Scan
            // HINT: vxo_points is what you want
        }






        usleep (1000000/fps);
    }
    
    

    return NULL;
}

state_t *
state_create (void)
{
    state_t *state = calloc (1, sizeof(*state));

    state->vw = vx_world_create ();
    state->vxeh = calloc (1, sizeof(*state->vxeh));
    state->vxeh->key_event = key_event;
    state->vxeh->mouse_event = mouse_event;
    state->vxeh->touch_event = touch_event;
    state->vxeh->dispatch_order = 100;
    state->vxeh->impl = state; // this gets passed to events, so store useful struct here!

    state->vxapp.display_started = rob550_default_display_started;
    state->vxapp.display_finished = rob550_default_display_finished;
    state->vxapp.impl = rob550_default_implementation_create (state->vw, state->vxeh);

    state->running = 1;

    return state;
}

void
state_destroy (state_t *state)
{
    if (!state)
        return;

    if (state->vxeh)
        free (state->vxeh);

    if (state->gopt)
        getopt_destroy (state->gopt);

    if (state->pg)
        pg_destroy (state->pg);

    free (state);
}

// This is intended to give you a starting point to work with for any program
// requiring a GUI. This handles all of the GTK and vx setup, allowing you to
// fill in the functionality with your own code.
int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    rob550_init (argc, argv);
    state_t *state = state_create ();

    printf("State created\n");

    state->pg = pg_create ();
    // Initialize this application as a remote display source. This allows
    // you to use remote displays to render your visualization. Also starts up
    // the animation thread, in which a render loop is run to update your display.
    vx_remote_display_source_t *cxn = vx_remote_display_source_create (&state->vxapp);

    printf("created display source\n");


    parameter_listener_t *my_listener = calloc (1, sizeof(*my_listener));
    my_listener->impl = state;
    my_listener->param_changed = my_param_changed;
    pg_add_listener (state->pg, my_listener);
    
    //init_trajectory();

    trajectory = zarray_create(2*sizeof(double));

    ellipse    = zarray_create(16*sizeof(double));

    // Launch our worker threads
    pthread_create (&state->animate_thread, NULL, animate_thread, state);

    printf("created animate thread\n");

    // This is the main loop
    rob550_gui_run (&state->vxapp, state->pg, 1024, 768);

    printf("running main loop");

   

    // Quit when GTK closes
    state->running = 0;
    pthread_join (state->animate_thread, NULL);

    // Cleanup

    zarray_destroy(trajectory);
    zarray_destroy(ellipse);
    state_destroy (state);
    vx_remote_display_source_destroy (cxn);
    vx_global_destroy ();
}
