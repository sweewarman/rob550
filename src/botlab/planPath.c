/* Author:  Christopher Lesch
 * Date:  4/1/2015
 * This file contains an implementation of the D* Lite algorithm as proposed by Sven Koenig.
 * Additionally, it contains an implementation of a Bezier Spline path smoothing algorithm.
 */
#include "dStarLite.h"
#include "smooth.h"
#include "priorityQueue.h"
#include <stdio.h>



path_t global;

void printStep(int step)
{
	printf("STEP %d\n",step);
	printf("g ----------------------------------\n");
	for(int i = 0; i < global.gridHeight; ++i) {
		for(int j = 0; j < global.gridWidth; ++j) {
			printf("%d\t",global.g[i][j]);
		}
		printf("\n");
	}
	printf("rhs -------------------------------\n");
	for(int i = 0; i < global.gridHeight; ++i) {
		for(int j = 0; j < global.gridWidth; ++j) {
			printf("%d\t",global.rhs[i][j]);
		}
		printf("\n");
	}
	printf("cost ------------------------------\n");
	for(int i = 0; i < global.gridHeight; ++i) {
		for(int j = 0; j < global.gridWidth; ++j) {
			printf("%d\t",global.cost[i][j]);
		}
		printf("\n");
	}
}

//pathPlan(double startX, double startY, double goalX, double goalY, int gridWidth, int gridHeight,
//		map_t *cost, double minPathDistance)

int main() {
	int path[100][2];
	int pathLength = 0;
	int index[2];
	//Initialize start and target locations.
	global.startLoc[0] = 0;
	global.startLoc[1] = 0;
	global.goalLoc[0] = 4;
	global.goalLoc[1] = 6;
	global.gridWidth = 10;
	global.gridHeight = 10;
	//Initialize
	initialize();
	//printStep(-1);
	//Compute the initial path.
	computeShortestPath();
	//printStep(pathLength);
	//Add this position to the path
	path[pathLength][0] = global.startLoc[0];
	path[pathLength++][1] = global.startLoc[1];
	//Create Path
	while(!(global.startLoc[0] == global.goalLoc[0] && global.startLoc[1] == global.goalLoc[1])) {
		if(global.rhs[global.startLoc[0]][global.startLoc[1]] == MAX_COST){
			return 0;
		}
		minSuccess(global.startLoc[0],global.startLoc[1],index);
		global.startLoc[0] = index[0];
		global.startLoc[1] = index[1];
		//Add this position to the path
		path[pathLength][0] = global.startLoc[0];
		path[pathLength++][1] = global.startLoc[1];
		//Compute the next path.
		computeShortestPath();
	//	printStep(pathLength);
	}

	for(int i = 0; i < pathLength; ++i){
		printf("Path[%d]: %d, %d\n",i,path[i][0],path[i][1]);
	}


	/*//Test for passing in using array like syntax.
	int index[2];
	index[0] = -1;
	index[1] = -1;
	minSuccess(global.startLoc,index);
	printf("Key[0] = %d Key[1] = %d\n",index[0],index[1]);*/
	/*

	int numDStarPoints = 4; //Number of points the path goes through.
	int numCoords = 2;  //2D data, x and y.
	int numControlPointsGenerated = numDStarPoints - 1;  //Num points for smooth.

	//Initialize path storage variable.
	double **xy;
	double **p1;
	double **p2;
	initializeArrays(numDStarPoints,numCoords,numControlPointsGenerated,&xy,&p1,&p2);

	//For testing, give some default control points.
	xy[0][0] = 60;
	xy[0][1] = 60;
	xy[1][0] = 220;
	xy[1][1] = 300;
	xy[2][0] = 420;
	xy[2][1] = 300;
	xy[3][0] = 700;
	xy[3][1] = 240;

	computeControlPoints(xy,p1,p2,numControlPointsGenerated);
	double **pathSeg;
	int pathLength = computeTrajectory(&pathSeg,xy,p1,p2,numControlPointsGenerated,numCoords);
	printf("PathLength: %d\n",pathLength);
	for(int i = 0; i < pathLength; ++i) {
		for(int j = 0; j < 2; ++j) {
			printf("pathSeg[%d][%d] = %f\n",i,j,pathSeg[i][j]);
		}
	}
	/* Print out values for testing.
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 2; j++) {
			printf("p1[%d][%d] = %f\n",i,j,p2[i][j]);
		}
	}
	

	freeArrays(numDStarPoints, numControlPointsGenerated, &xy,&p1,&p2);
	*/
}