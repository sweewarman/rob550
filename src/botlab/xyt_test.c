#include "xyt.h"
#include "math.h"


void print_matrix(const gsl_matrix* matrix){
  int i,j;

  printf("Number of Rows in Matrix: %d\n",(int)matrix->size1);
  printf("Number of Columns in Matrix: %d\n", (int)matrix->size2);

  // iterates through the matrix to print out each value
  for (i=0;i<(matrix->size1);i++){
    for (j=0;j<(matrix->size2);j++){
      printf("%f\t", gsl_matrix_get(matrix,i,j));
    }
    printf("\n");
  }
  printf("\n");

}


void print_vector(const gsl_vector* vector){
  int i;

  printf("Number of Elements in Vector: %d\n",(int)vector->size);

  // iterates through the matrix to print out each value
  for (i=0;i<(vector->size);i++){
      printf("%f\t", gsl_vector_get(vector,i));
      printf("\n");
  }
  printf("\n");

}


int main(){

  gsl_matrix *T_ij    = gsl_matrix_calloc(3,3);
  gsl_matrix *T_jk    = gsl_matrix_calloc(3,3);

  gsl_vector *X_ij    = gsl_vector_calloc(3);
  gsl_vector *X_jk    = gsl_vector_calloc(3);
  gsl_vector *X_ji    = gsl_vector_calloc(3);
  gsl_vector *X_ik    = gsl_vector_calloc(3);

  gsl_matrix *J_plus  = gsl_matrix_calloc(3,6);
  gsl_matrix *J_minus = gsl_matrix_calloc(3,3);
  gsl_matrix *J_tail  = gsl_matrix_calloc(3,6);

  printf("Test 1: \n\n");
  gsl_vector_set(X_ij,0,0);
  gsl_vector_set(X_ij,1,5);
  gsl_vector_set(X_ij,2,90*M_PI/180);
  printf("X_ij:\n");
  print_vector(X_ij);

  gsl_vector_set(X_jk,0,5);
  gsl_vector_set(X_jk,1,0);
  gsl_vector_set(X_jk,2,0);
  printf("X_jk:\n");
  print_vector(X_jk);

  printf("T_ij:\n");
  xyt_rbt_gsl(T_ij, X_ij);
  print_matrix(T_ij);

  printf("T_jk:\n");
  xyt_rbt_gsl(T_jk, X_jk);
  print_matrix(T_jk);
  
  xyt_head2tail_gsl(X_ik, J_plus, X_ij, X_jk);
  printf("J_plus:\n");
  print_matrix(J_plus);
  printf("X_ik:\n");
  print_vector(X_ik);

  xyt_inverse_gsl(X_ji,J_minus,X_ij);
  printf("J_minus:\n");
  print_matrix(J_minus);
  printf("X_ji:\n");
  print_vector(X_ji);

  xyt_tail2tail_gsl(X_jk, J_tail, X_ij,X_ik);
  printf("J_tail:\n");
  print_matrix(J_tail);
  printf("X_jk:\n");
  print_vector(X_jk);



  return 0;
}
