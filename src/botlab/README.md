INSTRUCTIONS
============

Running make in this directory compiles all the programs and puts the binaries inside the bin/ directory.

NOTE:

* To simplify the initialization process (to setup the procman deputies, sherriff, lcm tunnel etc) we've created two 
scripts. The setup_bot.sh script must be run on the maebot. This will set the required environmental variables, create 
the deputies and establish an lcm tunnel. The setup_lap.sh must be run on the lab laptop. This will setup all the 
necessary prerequisites and open up the procman sherriff.

* The odometry and lidar drivers must be run on the maebot prior to running the applications described below.

botlab_odometry
---------------
botlab_odometry is obtained from compiling the odometry.c program. We've programmed two different odometry models. 
The default model uses the data obtained from the wheel encoders to estimate the x,y position and the angle relative 
to an inertial frame. If the binary is executed with the "--use-gyro" flag, the second model that uses the wheel 
encoders and the gyro to determine the pose of the robot. The alpha and beta parameters for describing the covaraince 
in the longitudinal and lateral slip is specified as defines inside the odometry.c file. If the application is started 
with the "--use-gyro", the robot must remain stationary for the first 5 seconds. This enables us to obtain an estimate 
of the gyro bias and hence eliminate the gyro bias. 

botlab_scanmatcher
------------------
This app should be run after the gyro has been calibrated in the previous app. This program estimates poses based on 
scan matching and publishes these pose estimates over an LCM channel.

botlab_app
-----------------
This is the main engine that initiates mapping, planning and control. Running the VX remote viewer provides GUI 
for visualization. SHIFT+e turns on the PID controller. SHIFT+r turns off the PID controller.